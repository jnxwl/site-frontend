import Vue from "vue";
import VueRouter from "vue-router";
import Home from "@/views/Home/Home.vue";
import i18n from "@/plugins/i18n";
import { isAuth, logout } from "@/helpers/tokens";
import Cookie from "js-cookie";
import store, { accountModule } from "@/store";
import { chatsModule } from "@/views/User/modules/Chat/store/chats-module";

Vue.use(VueRouter);

const routes = [
  {
    path: "*",
    beforeEnter(to, from, next) {
      return to.path.startsWith("/crm") ? next() : next({ name: "NotFound" });
    },
  },
  {
    path: "/not-found",
    name: "NotFound",
    meta: {
      title: i18n.tc("titles.404"),
    },
    component: () =>
      import(/* webpackChunkName: "page-404" */ "@/views/404-Page/Page404"),
  },
  // ----- Страницы авторизации -----
  {
    path: "/login",
    name: "Auth",
    component: () =>
      import(/* webpackChunkName: "page-login" */ "@/views/Auth/Auth"),
    meta: {
      title: i18n.tc("titles.login"),
    },
  },
  {
    path: "/signup",
    name: "RegistrationUser",
    component: () =>
      import(
        /* webpackChunkName: "page-signup" */
        "@/views/Registration/RegistrationUser"
      ),
    meta: {
      title: i18n.tc("titles.clientRegister"),
    },
  },
  {
    path: "/business",
    name: "RegistrationClinic",
    component: () =>
      import(
        /* webpackChunkName: "page-business" */ "@/views/Registration/RegistrationClinic"
      ),
    meta: {
      title: i18n.tc("titles.bussinesRegister"),
    },
  },
  {
    path: "/recovery-password",
    name: "RecoveryPassword",
    component: () =>
      import(
        /* webpackChunkName: "page-recovery-password" */ "@/views/RecoveryPassword/RecoveryPassword"
      ),
    meta: {
      title: i18n.tc("titles.recoveryPassword"),
    },
  },
  // ----- Основные страницы -----
  {
    path: "/",
    name: "Home",
    component: Home,
    meta: {
      title: i18n.tc("titles.home"),
    },
  },
  {
    path: "/search",
    name: "Search",
    meta: {
      title: i18n.t("titles.search"),
    },
    component: () =>
      import(/* webpackChunkName: "page-search" */ "@/views/Search/Search"),
  },
  {
    path: "/services",
    name: "Services",
    meta: {
      title: i18n.tc("titles.services"),
    },
    component: () =>
      import(
        /* webpackChunkName: "page-services" */ "@/views/Services/Services"
      ),
  },
  {
    path: "/specialists",
    name: "Specialists",
    meta: {
      title: i18n.tc("titles.specialists"),
    },
    component: () =>
      import(
        /* webpackChunkName: "page-specialists" */ "@/views/Specialists/Specialists"
      ),
  },
  {
    path: "/specialists/:categoryId",
    name: "SpecialistsInner",
    meta: {
      title: i18n.tc("titles.specialists"),
    },
    component: () =>
      import(
        /* webpackChunkName: "page-specialists" */ "@/views/SpecialistsInner/SpecialistsInner"
      ),
  },
  {
    path: "/specialist/:id",
    name: "SpecialistOne",
    component: () =>
      import(
        /* webpackChunkName: "page-specialist" */ "@/views/SpecialistOne/SpecialistOne"
      ),
  },

  // ----- Вспомогательные страницы -----
  {
    path: "/about",
    name: "About",
    meta: {
      title: i18n.tc("titles.about"),
    },
    component: () =>
      import(/* webpackChunkName: "about" */ "@/views/About/About"),
  },
  {
    path: "/tariffs",
    name: "Tariff",
    meta: {
      title: i18n.tc("titles.tariff"),
    },
    component: () =>
      import(/* webpackChunkName: "tariff" */ "@/views/Tariff/Tariff"),
  },
  {
    path: "/teasers",
    name: "Teasers",
    meta: {
      title: i18n.tc("titles.teasers"),
    },
    component: () =>
      import(/* webpackChunkName: "teas-news" */ "@/views/Teasers/Teasers"),
  },
  {
    path: "/media",
    name: "Media",
    meta: {
      title: i18n.tc("titles.media"),
    },
    component: () =>
      import(/* webpackChunkName: "media" */ "@/views/Media/Media"),
  },

  // ----- Аккаунт -----
  {
    path: "/user",
    component: () => import(/* webpackChunkName: "user" */ "@/views/User/User"),
    meta: {
      requiresAuth: true,
    },
    children: [
      { path: "", redirect: { name: "Profile" } },
      {
        path: "profile",
        name: "Profile",
        meta: {
          requiresAuth: true,
          title: i18n.tc("titles.profile"),
        },
        component: () =>
          import(
            /* webpackChunkName: "profile" */ "@/views/User/modules/Profile/Profile"
          ),
      },
      {
        path: "orders",
        name: "Orders",
        meta: {
          requiresAuth: true,
          title: i18n.tc("titles.orders"),
        },
        component: () =>
          import(
            /* webpackChunkName: "orders" */ "@/views/User/modules/Orders/Orders"
          ),
      },

      {
        path: "internal-account",
        name: "InternalAccount",
        meta: {
          requiresAuth: true,
          title: i18n.tc("titles.internal"),
        },
        component: () =>
          import(
            /* webpackChunkName: "internal-account" */ "@/views/User/modules/InternalAccount/InternalAccount"
          ),
      },

      {
        path: "transaction",
        name: "Transactions",
        meta: {
          requiresAuth: true,
          title: i18n.tc("titles.transaction"),
        },
        component: () =>
          import(
            /* webpackChunkName: "transaction" */ "@/views/User/modules/Transactions/Transactions"
          ),
      },

      {
        path: "reviews",
        name: "Reviews",
        meta: {
          requiresAuth: true,
          title: i18n.tc("titles.reviews"),
        },
        component: () =>
          import(
            /* webpackChunkName: "reviews" */ "@/views/User/modules/Reviews/Reviews"
          ),
      },

      {
        path: "chat",
        name: "ProfileChats",
        meta: {
          requiresAuth: true,
          title: i18n.tc("titles.profileСhat"),
        },
        component: () =>
          import(
            /* webpackChunkName: "chats" */ "@/views/User/modules/Chat/Chats"
          ),
      },

      {
        path: "chat/:id",
        name: "ProfileChat",
        meta: {
          requiresAuth: true,
          title: i18n.tc("titles.profileСhat"),
        },
        component: () =>
          import(
            /* webpackChunkName: "chat" */ "@/views/User/modules/Chat/Chat"
          ),
      },

      {
        path: "referral-system",
        name: "ReferralSystem",
        meta: {
          requiresAuth: true,
          title: i18n.tc("titles.referral"),
        },
        component: () =>
          import(
            /* webpackChunkName: "referral-system" */ "@/views/User/modules/ReferralSystem/ReferralSystem"
          ),
      },
      {
        path: "favorite",
        name: "FavoritePage",
        meta: {
          requiresAuth: true,
          title: i18n.tc("titles.favorite"),
        },
        component: () =>
          import(
            /* webpackChunkName: "favorite" */ "@/views/User/modules/Favorites/Favorites"
          ),
      },

      {
        path: "notes",
        name: "Notes",
        redirect: { name: "NotesTab", params: { tab: "all" } },
      },

      {
        path: "notes/:tab",
        name: "NotesTab",
        meta: {
          requiresAuth: true,
          title: i18n.tc("titles.notes"),
        },
        component: () =>
          import(
            /* webpackChunkName: "notes" */ "@/views/User/modules/Notes/Notes"
          ),
      },

      {
        path: "status",
        name: "Status",
        meta: {
          requiresAuth: true,
          title: i18n.tc("titles.notes"),
        },
        component: () =>
          import(
            /* webpackChunkName: "status" */ "@/views/User/modules/Status/Status"
          ),
      },
    ],
  },

  {
    path: "/conditions",
    name: "Terms",
    meta: {
      title: i18n.t("titles.terms"),
    },
    component: () =>
      import(/* webpackChunkName: "page-terms" */ "@/views/Terms/Terms"),
  },
  {
    path: "/public-offer",
    name: "PublicOffer",
    meta: {
      title: i18n.t("titles.publicOffer"),
    },
    component: () =>
      import(
        /* webpackChunkName: "page-terms-business" */ "@/views/PublicOffer/PublicOffer"
      ),
  },
  {
    path: "/privacy",
    name: "Privacy",
    meta: {
      title: i18n.t("titles.privacy"),
    },
    component: () =>
      import(/* webpackChunkName: "page-privacy" */ "@/views/Privacy/Privacy"),
  },
  {
    path: "/contacts",
    name: "Contacts",
    meta: {
      title: i18n.t("titles.contacts"),
    },
    component: () =>
      import(
        /* webpackChunkName: "page-contacts" */ "@/views/Contacts/Contacts"
      ),
  },
  {
    path: "/blog",
    name: "Blog",
    meta: {
      title: i18n.t("titles.blog"),
    },
    component: () =>
      import(/* webpackChunkName: "page-blog" */ "@/views/Blog/Blog"),
  },
  {
    path: "/blog/:id",
    name: "BlogInner",
    meta: {
      title: i18n.t("titles.blog"),
    },
    component: () =>
      import(/* webpackChunkName: "page-blog-inner" */ "@/views/BlogInner/BlogInner"),
  },
  {
    path: "/:id([A-z0-9-]+)",
    name: "Salon",
    component: () =>
      import(/* webpackChunkName: "page-salon" */ "@/views/Salon/Salon"),
  },
];

const router = new VueRouter({
  mode: "history",
  routes,
  scrollBehavior(to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition;
    } else if (to.params.savedPosition) {
      return to.params.savedPosition;
    }
    if (to.hash) {
      return { selector: to.hash, offset: { y: 100 } };
    }
    return { x: 0, y: 0 };
  },
});

router.beforeEach(async (to, from, next) => {
  if (to.name === from.name) return next();

  const isAuthorized = await isAuth();
  const isAuthRequired = to.matched.some((record) => record.meta.requiresAuth);
  const redirectUrl = to.query.redirectUrl;

  if (
    isAuthRequired &&
    /*process.env.NODE_ENV === "production" &&*/ !isAuthorized
  ) {
    logout();
    Vue.prototype.$doNoty.error(i18n.tc("helpers.notAuth"));
    return next(`/login?redirectUrl=${to.path}`);
  }

  if (isAuthorized && !store.getters["User/user"]) {
    await store.dispatch("User/getUserInfo", {
      accessToken: Cookie.get("accessToken"),
    });
    await accountModule.loadUserInfo();
  }

  if (isAuthorized) {
    await chatsModule.init();
  }

  if (!isAuthorized) return next();
  if (to.path === redirectUrl) return next();
  return redirectUrl ? next({ path: redirectUrl }) : next();
});

export default router;
