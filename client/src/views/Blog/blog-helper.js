import { random } from "lodash";

const themes = [
  {
    color: "#CEB5E4",
    backgroundColor: "rgba(206, 181, 228, 0.2)",
  },
  {
    color: "#A8D3DC",
    backgroundColor: "rgba(168, 211, 220, 0.2)",
  },
  {
    color: "#DBA2A0",
    backgroundColor: "rgba(219, 162, 160, 0.2)",
  },
];

export const getTagTheme = () => themes[random(0, themes.length - 1)];
