import { VuexModule, Module, Action, Mutation } from "vuex-class-modules";

import store from "@/store";
import { paymentsApi } from "@/services/api/payments-api";
import { mainApi } from "@/services/api/main-api";

import { Transaction } from "@/models/Transaction";

import Cookie from "js-cookie";

@Module({ generateMutationSetters: true })
class TransactionsModule extends VuexModule {
  isLoading = false;
  isOrdersLoading = false;

  _transactions = [];
  _transactionsOffset = 0;
  _orders = [];

  canLoadMore = true;

  get transactions() {
    return this._transactions.map((item) => {
      const order = this._orders.find(
        (order) => order.id === +item.data?.order_id
      );
      return {
        ...new Transaction(item),
        salonName: order?.salon?.data?.title || null,
        serviceName: order?.specialist_service?.service?.title || null,
      };
    });
  }

  get transactionsAmount() {
    return this.transactions.reduce((acc, item) => acc + item.amount, 0);
  }

  @Mutation
  setTransactionsOffset(value) {
    this._transactionsOffset = value;
  }

  @Action
  async loadTransactions(loadNext = false) {
    this.isLoading = true;

    if (!loadNext) {
      this.canLoadMore = true;
    }

    try {
      const { data } = await paymentsApi.getTransactions({
        limit: 8,
        access_token: Cookie.get("accessToken"),
        offset: this._transactionsOffset,
        sort: [
          {
            timestamp: -1,
          },
        ],
      });

      if (!data.ok) {
        throw data.message;
      }

      this.setTransactionsOffset(this._transactionsOffset + 1);
      if (loadNext) {
        this._transactions = this._transactions.concat(data.transactions);
      } else {
        this._transactions = data.transactions;
      }

      if (data.transactions?.length < 8) {
        this.canLoadMore = false;
      }
      this.loadTransactionsOrders(data.transactions);
    } catch (err) {
      store._vm.$doNoty.error(err);
    }
    this.isLoading = false;
  }

  @Action
  async loadTransactionsOrders(transactions = []) {
    this.isOrdersLoading = true;

    try {
      const orderIds = transactions
        .map((item) => +item.data.order_id)
        .filter((item) => item);

      const request = {};
      orderIds.forEach((item, index) => {
        request[`id[${index}]`] = item;
      });

      const { data } = await mainApi.getOrders(request);

      this._orders = this._orders.concat(data.data || []);
    } catch (err) {
      store._vm.$doNoty.error(err);
    }

    this.isOrdersLoading = false;
  }
}

export const transactionsModule = new TransactionsModule({
  store,
  name: "transactions",
});
