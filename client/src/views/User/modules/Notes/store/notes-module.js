import { VuexModule, Module, Action } from "vuex-class-modules";
import store from "@/store";
import { Note } from "@/models/Note";
import { notyApi } from "@/services/api/noty-api";
import { bellModule } from "@/store/index";

const NOTES_PAGE_SIZE = 20;

@Module({ generateMutationSetters: true })
class NotesModule extends VuexModule {
  isLoading = false;
  hasNext = true;
  _notes = [{}];

  get notes() {
    return this._notes.map((item) => new Note(item));
  }

  @Action
  async loadNotes(subject) {
    this.isLoading = true;
    this.hasNext = true;
    this._notes = [];

    try {
      const { data } = await notyApi.getInboxMessages({
        limit: NOTES_PAGE_SIZE,
        offset: this._notes.length,
        subject,
      });
      this._notes = data.items;
      this.hasNext = this._notes.length >= NOTES_PAGE_SIZE;
      setTimeout(() => this.markRead(data.items), 2000);
    } catch (err) {
      store._vm.$doNoty.error(err);
    }

    this.isLoading = false;
  }

  async loadMoreNotes() {
    this.isLoading = true;
    const { data } = await notyApi.getInboxMessages({
      limit: NOTES_PAGE_SIZE,
      offset: this._notes.length,
    });
    if (!data.items.length) {
      this.hasNext = false;
      this.isLoading = false;
      return;
    }

    this._notes.push(...data.items);
    setTimeout(() => this.markRead(data.items), 2000);
    this.isLoading = false;
  }

  @Action
  async markRead(items) {
    await notyApi.markMessageRead({
      msg_ids: items.map((item) => item.id),
    });
    await bellModule.loadUnreadCount();

    this._notes.forEach((note, i) => {
      this._notes.splice(i, 1, { ...note, read: true });
    });
  }
}

export const notesModule = new NotesModule({ store, name: "notes" });
