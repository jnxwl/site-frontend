import { VuexModule, Module, Action, Mutation } from "vuex-class-modules";
import { Order } from "@/models/Order";
import { Review } from "@/models/Review";
import { mainApi } from "@/services/api/main-api";
import store, { accountModule } from "@/store";

const ORDERS_STATUSES = {
  booked: {
    "status[0]": "booked_customer_price",
    "status[1]": "booked_without_cancel",
    "status[2]": "booked_with_cancel",
    "status[3]": "payed_customer_price",
    "status[4]": "payed_without_cancel",
    "status[5]": "payed_with_cancel",
    "status[6]": "new",
  },
  canceled: {
    "status[0]": "user-cancel",
    "status[1]": "salon-cancel",
    "status[2]": "cancel",
  },
  completed: {
    status: "complete",
  },
};

@Module({ generateMutationSetters: true })
class OrdersModule extends VuexModule {
  isCanceledLoading = false;
  isCompletedLoading = false;
  isBookedLoading = false;

  _orders = [];
  _canceledOrders = [];
  _completedOrders = [];
  _bookedOrders = [];
  _reviews = [];

  canceledOrdersCount = 0;
  completedOrdersCount = 0;
  bookedOrdersCount = 0;

  canceledOrdersAmount = 0;
  completedOrdersAmount = 0;
  bookedOrdersAmount = 0;

  selectedOrder = null;

  get orders() {
    return this._orders.map((item) => new Order(item));
  }

  get pendingOrders() {
    return this._bookedOrders.map((item) => new Order(item));
  }

  get completedOrders() {
    return this._completedOrders.map((item) => new Order(item));
  }

  get canceledOrders() {
    return this._canceledOrders.map((item) => new Order(item));
  }

  get getReview() {
    return (id) => {
      const review = this._reviews.find((item) => item.id === id);
      return review ? new Review(review) : null;
    };
  }

  @Mutation
  setSelectedOrder(order) {
    this.selectedOrder = order;
  }

  @Action
  async loadOrders() {
    this.isCanceledLoading = true;
    this.isCompletedLoading = true;
    this.isBookedLoading = true;

    await this.loadCanceledOrders();
    this.isCanceledLoading = false;
    await this.loadBookedOrders();
    this.isBookedLoading = false;
    await this.loadCompletedOrders();
    this.isCompletedLoading = false;
  }

  @Action
  async loadCanceledOrders(loadNext = false) {
    this.isCanceledLoading = true;

    try {
      const { data } = await mainApi.getOrders({
        user_id: accountModule.userId,
        limit: 3,
        ...ORDERS_STATUSES.canceled,
        offset: loadNext ? Math.floor(this._canceledOrders.length / 3) : 0,
        "order_by[start_at]": "desc",
      });

      if (loadNext) {
        this._canceledOrders = this._canceledOrders.concat(data.data);
      } else {
        this.canceledOrdersCount = data.count;
        this.canceledOrdersAmount = data.priceSum;
        this._canceledOrders = data.data;
      }
    } catch (err) {
      store._vm.$doNoty.error(err);
    }

    this.isCanceledLoading = false;
  }

  @Action
  async loadCompletedOrders(loadNext = false) {
    this.isCompletedLoading = true;

    try {
      const { data } = await mainApi.getOrders({
        user_id: accountModule.userId,
        limit: 3,
        ...ORDERS_STATUSES.completed,
        offset: loadNext ? Math.floor(this._completedOrders.length / 3) : 0,
        "order_by[start_at]": "desc",
      });

      if (loadNext) {
        this._completedOrders = this._completedOrders.concat(data.data);
      } else {
        this.completedOrdersCount = data.count;
        this.completedOrdersAmount = data.priceSum;
        this._completedOrders = data.data;
      }
    } catch (err) {
      store._vm.$doNoty.error(err);
    }

    this.isCompletedLoading = false;
  }

  @Action
  async loadBookedOrders(loadNext = false) {
    this.isBookedLoading = true;

    try {
      const { data } = await mainApi.getOrders({
        user_id: accountModule.userId,
        limit: 3,
        ...ORDERS_STATUSES.booked,
        offset: loadNext ? Math.floor(this._bookedOrders.length / 3) : 0,
        "order_by[start_at]": "asc",
      });

      if (loadNext) {
        this._bookedOrders = this._bookedOrders.concat(data.data);
      } else {
        this.bookedOrdersCount = data.count;
        this.bookedOrdersAmount = data.priceSum;
        this._bookedOrders = data.data;
      }
    } catch (err) {
      store._vm.$doNoty.error(err);
    }

    this.isBookedLoading = false;
  }

  @Action
  async updateCanceledOrders() {
    this.isCanceledLoading = true;

    try {
      const { data } = await mainApi.getOrders({
        user_id: accountModule.userId,
        limit: this._canceledOrders.length,
        ...ORDERS_STATUSES.canceled,
      });

      this.canceledOrdersCount = data.count;
      this.canceledOrdersAmount = data.priceSum;
      this._canceledOrders = data.data;
    } catch (err) {
      store._vm.$doNoty.error(err);
    }

    this.isCanceledLoading = false;
  }

  @Action
  async updateCompletedOrders() {
    this.isCompletedLoading = true;

    try {
      const { data } = await mainApi.getOrders({
        user_id: accountModule.userId,
        limit: this._completedOrders.length,
        ...ORDERS_STATUSES.completed,
      });

      this.completedOrdersCount = data.count;
      this.completedOrdersAmount = data.priceSum;
      this._completedOrders = data.data;
    } catch (err) {
      store._vm.$doNoty.error(err);
    }

    this.isCompletedLoading = false;
  }

  @Action
  async updateBookedOrders() {
    this.isBookedLoading = true;

    try {
      const { data } = await mainApi.getOrders({
        user_id: accountModule.userId,
        limit: this._bookedOrders.length,
        ...ORDERS_STATUSES.booked,
      });

      this.bookedOrdersCount = data.count;
      this.bookedOrdersAmount = data.priceSum;
      this._bookedOrders = data.data;
    } catch (err) {
      store._vm.$doNoty.error(err);
    }

    this.isBookedLoading = false;
  }

  @Action
  async cancelOrder() {
    try {
      await mainApi.cancelOrder(this.selectedOrder.id);
      store._vm.$doNoty.success("Бронирование успешно отменено");
      this.loadOrders();
    } catch (err) {
      store._vm.$doNoty.error(err);
    } finally {
      this.selectedOrder = null;
    }
  }

  @Action
  async complainOrder() {
    try {
      await mainApi.complainOrder(this.selectedOrder.id);
      store._vm.$doNoty.success("Жалоба на обман принята");
      this.loadOrders();
    } catch (err) {
      store._vm.$doNoty.error(err);
    } finally {
      this.selectedOrder = null;
    }
  }

  @Action
  async postponeOrder({ startAt, finishAt }) {
    try {
      await mainApi.updateOrder(this.selectedOrder.id, {
        start_at: startAt,
        finish_at: finishAt,
        price: this.selectedOrder.price,
      });
      store._vm.$doNoty.success("Бронирование успешно перенесено");
      this.loadOrders();
    } catch (err) {
      store._vm.$doNoty.error(err);
    } finally {
      this.selectedOrder = null;
    }
  }

  @Action
  async updateOrder({ order, data }) {
    try {
      await mainApi.updateOrder(order.id, {
        start_at: order.startAt.toISOString(),
        finish_at: order.finishAt.toISOString(),
        price: order.price,
        status: order.status,
        data,
      });
    } catch (err) {
      store._vm.$doNoty.error(err);
    }
  }

  @Action
  async loadReview({ order }) {
    try {
      if (this.getReview(order.reviewId)) return;
      const { data } = await mainApi.getReviews({
        user_id: accountModule.userId,
        salon_id: order.salonId,
      });
      this._reviews = this._reviews.concat(data.data);
    } catch (err) {
      store._vm.$doNoty.error(err);
    }
  }
}

export const ordersModule = new OrdersModule({ store, name: "orders" });
