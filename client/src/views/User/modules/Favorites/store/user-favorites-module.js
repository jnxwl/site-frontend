import { VuexModule, Module, Action } from "vuex-class-modules";
import store from "@/store";
import { mainApi } from "@/services/api/main-api";

import { Salon } from "@/models/Salon";
import { Specialist } from "@/models/Specialist";
import { SpecialistService } from "@/models/SpecialistService";
import { favoritesModule, accountModule } from "@/store/index";

@Module({ generateMutationSetters: true })
class UserFavoritesModule extends VuexModule {
  _salons = [];
  _specialists = [];
  _services = [];
  _specialistsServices = [];

  isSalonsLoading = false;
  isSpecialistsLoading = false;
  isServicesLoading = false;

  loadedSalons = 0;
  loadedSpecialists = 0;
  loadedServices = 0;

  canLoadMoreSalons = true;
  canLoadMoreSpecialists = true;
  canLoadMoreServices = true;

  get salons() {
    return this._salons.map((item) => new Salon(item));
  }

  get specialists() {
    return this._specialists.map((item) => new Specialist(item));
  }

  get services() {
    return this._services
      .filter((item) => this.favoriteServicesIds.includes(item.id))
      .map((item) => new SpecialistService(item));
  }

  get favoriteServicesIds() {
    return accountModule.favoriteServices
      .reduce((acc, item) => acc.concat(item.items), []);
  }

  @Action
  async init() {
    try {
      await Promise.all([
        this.loadSalons(),
        this.loadSpecialists(),
      ]);
      await this.loadServices()
    } catch (err) {
      store._vm.$doNoty.error(err);
    }
  }

  @Action
  async loadSalons(loadNext = false) {
    if (!favoritesModule.favoriteSalons.length) {
      this._salons = [];
      return;
    }

    if (!loadNext) {
      this._salons = [];
      this.loadedSalons = 0;
      this.canLoadMoreSalons = true;
    }

    this.isSalonsLoading = true;

    try {

      const { data } = await mainApi.getSalons({
        id: favoritesModule.favoriteSalons.slice(
          this.loadedSalons, this.loadedSalons + 3
        ),
      });

      const { data: stats } = await mainApi.getReviewsStatistics({
        salon_id: data.data.map((item) => item.id),
      });

      const salons = data.data.map((item) => ({
        ...(stats.find((s) => s.id === item.id)?.statistic || {}),
        ...item,
      }));

      if (loadNext) {
        this._salons = this._salons.concat(salons);
      } else {
        this._salons = salons;
      }

      this.loadedSalons += 3;

      if (this.loadedSalons >= accountModule.favoriteSalons.length) {
        this.canLoadMoreSalons = false;
      }
    } catch (err) {
      store._vm.$doNoty.error(err);
    }

    this.isSalonsLoading = false;
  }

  @Action
  async loadSpecialists(loadNext = false) {
    if (!accountModule.favoriteSpecialists?.length) {
      this._specialists = [];
      return;
    }

    if (!loadNext) {
      this._specialists = [];
      this.canLoadMoreSpecialists = true;
      this.loadedSpecialists = 0;
    }

    try {
      const { data } = await mainApi.getSpecialists({
        id: accountModule.favoriteSpecialists.slice(
          this.loadedSpecialists, this.loadedSpecialists + 3
        ),
      });
      const { data: stats } = await mainApi.getReviewsStatistics({
        specialist_id: data.data.map((item) => item.id),
      });
      const specialists = data.data.map((item) => ({
        ...((stats || []).find((s) => s.id === item.id)?.statistic || {}),
        ...item,
      }));

      if (loadNext) {
        this._specialists = this._specialists.concat(specialists);
      } else {
        this._specialists = specialists;
      }
      this.loadedSpecialists += 3;
      if (this.loadedSpecialists >= accountModule.favoriteSpecialists.length) {
        this.canLoadMoreSpecialists = false;
      }
    } catch (err) {
      store._vm.$doNoty.error(err);
    }
  }

  @Action
  async loadServices(loadNext = false) {
    if (!accountModule.favoriteServices?.length) {
      this._services = [];
      return;
    }

    if (!loadNext) {
      this._services = [];
      this.canLoadMoreServices = true;
      this.loadedServices = 0;
    }

    try {
      const { data } = await mainApi.getSpecialistServices({
        specialist_id: accountModule.favoriteServices
          .slice(this.loadedServices, this.loadedServices + 3)
          .map((item) => item.specialistId),
      });
      const { data: stats } = await mainApi.getReviewsStatistics({
        specialist_service_id: data.data.map((item) => item.id),
      });

      const services = data.data.map((item) => ({
        ...((stats || []).find((s) => s.id === item.id)?.statistic || {}),
        ...item,
      }));
      if (loadNext) {
        this._services = this._services.concat(services);
      } else {
        this._services = services;
      }
      this.loadedServices += 3;
      if (this.loadedServices >= accountModule.favoriteServices.length) {
        this.canLoadMoreServices = false;
      }
    } catch (err) {
      store._vm.$doNoty.error(err);
    }
  }

  @Action
  removeSalon(salonId) {
    favoritesModule.setFavoriteSalon({
      salonId,
      isFavorite: false,
    });

    this._salons = this._salons.filter((item) => item.id !== salonId);
  }

  @Action
  removeSpecialist(specialistId) {
    favoritesModule.setFavoriteSpecialist({
      specialistId,
      isFavorite: false,
    });

    this._specialists = this._specialists.filter(
      (item) => item.id !== specialistId
    );
  }

  @Action
  removeService({ serviceId, specialistId }) {
    favoritesModule.setFavoriteService({
      serviceId,
      specialistId,
      isFavorite: false,
    });

    this._services = this._services.filter((item) => item.id !== serviceId);
  }
}

export const userFavoritesModule = new UserFavoritesModule({
  store,
  name: "user-favorites",
});
