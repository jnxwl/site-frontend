import { VuexModule, Module, Action } from "vuex-class-modules";
import store from "@/store";
import { Review } from "@/models/Review";
import { accountModule } from "@/store/index";
import { mainApi } from "@/services/api/main-api";

@Module({ generateMutationSetters: true })
class ReviewsModule extends VuexModule {
  isLoading = false;
  _reviews = [];

  get reviews() {
    return this._reviews.map((item) => new Review(item));
  }

  @Action
  async loadReviews() {
    this.isLoading = true;
    this._reviews = [];

    try {
      const { data: reviews } = await mainApi.getReviews({
        user_id: accountModule.userId,
        limit: 100,
      });
      this._reviews = reviews.data;
    } catch (err) {
      store._vm.$doNoty.error(err);
    }

    this.isLoading = false;
  }
}

export const reviewsModule = new ReviewsModule({ store, name: "reviews" });
