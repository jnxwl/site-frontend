export const CHAT_SORTS = Object.freeze({
  online: "online",
  nameAsc: "name_asc",
  nameDesc: "name_desc",
});
