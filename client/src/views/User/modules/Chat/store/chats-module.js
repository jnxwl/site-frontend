import { VuexModule, Module, Action } from "vuex-class-modules";

import i18n from "@/plugins/i18n";

import store from "@/store";
import router from "@/router";

import { mainApi } from "@/services/api/main-api";
import { ChatService } from "@/services/chat";

import { Chat } from "@/models/Chat";
import { ChatMessage } from "@/models/ChatMessage";
import { Specialist } from "@/models/Specialist";
import { accountModule } from "@/store/index";

const chatService = new ChatService();

@Module({ generateMutationSetters: true })
class ChatsModule extends VuexModule {
  _eventListenersSet = false;
  _initialized = false;
  _isOnline = false;
  _chatsLoaded = false;
  _chatsIsLoading = false;
  _chats = [];
  _currentChatId = null;

  get online() {
    return this._isOnline;
  }

  get chatsLoaded() {
    return this._chatsLoaded;
  }

  get chatsIsLoading() {
    return this._chatsIsLoading;
  }

  get userChats() {
    return this._chats
      .filter(
        (chat) =>
          chat.status !== Chat.states.pending &&
          chat.status !== Chat.states.closed
      )
      .map((chat) => new Chat(chat));
  }

  get newChats() {
    return this._chats
      .filter((chat) => chat.status === Chat.states.pending)
      .map((chat) => new Chat(chat));
  }

  get chatIndex() {
    return this.findChatIndex("id", this._currentChatId);
  }

  get chat() {
    if (this.chatIndex > -1) {
      return new Chat(this._chats[this.chatIndex]);
    }
    return new Chat({});
  }

  get messages() {
    if (this.chatIndex > -1) {
      return this._chats[this.chatIndex].messages.map(
        (item) => new ChatMessage(item)
      );
    }
    return [];
  }

  onError(e) {
    console.log("[Chat] error", e);
  }

  onOnline() {
    this._isOnline = true;
  }

  onOffline() {
    this._isOnline = false;
  }

  onChat({ detail: chat }) {
    console.log("onChat", chat);
    this.processChat(chat, true);
  }

  onMessage({ detail: message }) {
    const chatIndex = this.findChatIndex("id", message.chat_id);
    if (chatIndex > -1) {
      const processedMessage = this.processMessage(message);
      this._chats[chatIndex].messages.push(processedMessage);
      const chatPath = `/user/chat/${message.chat_id}`;
      if (router.currentRoute.path !== chatPath) {
        store._vm.$doNoty.info(
          i18n.t("chat.new_message", {
            user: this._chats[chatIndex].specialist_name,
          }),
          () => router.push(`/user/chat/${message.chat_id}`)
        );
      }
    }
  }

  onPresence({ detail: presence }) {
    const chatIndex = this.findChatIndex("specialist_id", presence.user_id);
    if (chatIndex > -1) {
      this._chats[chatIndex].specialist_presence = presence.status;
    }
  }

  setEventListeners() {
    if (this._eventListenersSet) return;

    chatService.addEventListener("chat:error", this.onError);
    chatService.addEventListener("chat:online", this.onOnline);
    chatService.addEventListener("chat:offline", this.onOffline);
    chatService.addEventListener("chat:chat", this.onChat);
    chatService.addEventListener("chat:message", this.onMessage);
    chatService.addEventListener("chat:presence", this.onPresence);
    this._eventListenersSet = true;
  }

  findChatIndex(searchIn, searchFor) {
    return this._chats.findIndex((chat) => chat[searchIn] === searchFor);
  }

  processMessage(msg) {
    return {
      user_id: msg.sender,
      text: msg.content,
      created_at: msg.timestamp,
      is_system: msg.direction === 0,
      is_outbound: msg.direction === 1,
    };
  }

  async processChats(chats) {
    const promises = chats.map((chat) => {
      return new Promise((resolve, reject) => {
        // TODO: Заменить метод получения данных о пользователе на универсальный
        if (accountModule.roles.includes("specialist")) {
          const avatar =
            "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACUAAAAlCAYAAADFniADAAALNklEQVRYR51YCXhU1RX+3yxvtkwyWcgGIqOyKFLRtLJEYihgPqtCoUGloKWofFSIAhVFa+X7WjYtiiiyCWgV0oAV8QOaWqgUpQhWBZWWIKYQAolAZiazr++9nnMn2yQhak++JPe9++65557lP+dcSSOKRqMIhULIzMxEIpGA3+8XY1VV4fV64XA4wNTc3Iz09HS4XS5UbtuC2rOnYE+zYUTRKIwePRY2mw0ejweyLLeNDQaDGAcDAUCSYLfbBU/mzXvwXrwnj1kGlkWKRCIsl/hIr9ej45gFURRFvGcKBPyY9vDPIDujSMs1QaMfiX6YNAWIn7Jhw7NbYTabxUZPL16AOvVL2HKNgE5D1K/A7uqD1cs3kXyS4Mv78p4dx5Lb7dZMJhOsVqs4JZ+MT8NjnU6HjIwMoaGTX9Vg0esVsOYYYHHIQpDuKOYlDbivQvDKOiFId+Q/F8fbzx6gQwbEXrwn78cysCzSdzGf2+1Cxaop0JslZORbhYZSiB+TChPkr4/DfgVp5zLE2o3XZGDbhne7N188HtdY1fQfFotFmCsWiwkTsFr5/YSHb4UpW4Kjj1WYKx5ScPHLELL7WyFn6Nh4iAc1eGojMGcakO7Uw3c6gYx+JjGXInGLoFoCeH3uXmFm3pP35r34+VvNt2zF73E89AFsOTIMJr0QJnewDaqiIfg1kGfti7zs3mj2N6G+uRaWK1X4G2JoqgkjrYCcPE+Gya6HXtZB1VQkQhrC7gSingSmls3EtKm/ECbr0XwsLduaI441dUfFcOgtQEYh/WmhYI0BVS/tEdpk+ut71Vi3dg0GDhqEpUuWY2vlm9h5fBMSERX2Kw1Jk9YpuPWqu3BH2XiKtCw0Njbgvfer8eGJatiRjS3r3haRJ6KPVKexIBSFwulYfeEwnTItDTvf3YHX/vkH0pKJtJQ0k++Uil1rD7YJuHnzRry8+iWsXr0Gs341C3qdHseOfi5MMn7qOIQ0H/pmO/HHNW+leNiKFc/B5WrCokW/Q13dGVRWbcUTC54UJuzWfBwN0x++F+cu1sEx0Ch8KXTCiF62Qqx9/rU2iOBdSktvgcvjJU9Tcf3gwXQoBb+e/xiGDR8hhLjrl6Oxc+O+lDUlJcVYufJFTJ8xnawBfHz4Y8I2Ew4dOoiSklJIhBMaOzZrh8P/7Nk6zFw6GUYbObRfg+yQkF5AuFPjQNX6dwW+dKTFS36Hbdu3Y+SwYdBoqiAvH+MnTERR0Q/FZ+wKTKx5JnaJHwwdArNJhqICW7dswYYN67HyhVWort6DkSNvSRWKzVf20M0wOdhURLTImiVDTjMko+5oFra9uSNFKH6YfE85TtacIKF0uP66wfhTZVXKNytfXIF5cx9re1dyazGG3jgUB/5xAOPGjENxcTEmTionePDRoXWp5ntw9v1oNJ1IYZjRx9KG2oHzCdyUPRqLnlrcRbBWLXTWJL9f/uwyLHziyZQ1zyx6Gh8dPoKZDzyAyXffK+ZOnqxB//4DIJFjC0cPBoO4f/HtBCntwMhhbM9NRlgrNX0ZQbqUg82rqkRgfBc6fOQjDB+W9LGeaNnSxZg955F2RH+w4j64006naqk3gWWqC4l5JaLhwrEQbBY7ZkyYjTvvHC+ScGfiSJ4yawK2vLJDYFFPtHbNy/j51PtFLhTRZzQaMXnhaOg68NXp2cHbsak7hhoBaNN/ItBiEgyyHha9HTlp+SDdwxVphNVJiVaW4D2hYNOSt5Cbm9eFzbFjn2He/Ll4a/sOoXmR+wistF27dmIz4VFHSs+zQGfsoKbus0XKGk2laiMuQUd4KSULizY6dzAA2WKE2WAWPhpJRBAJxLDgoWcwYnixKIkY24SmOCFPvK8MsV7uFCYOcvDWnKVEgJGfK6BEgb29Kbz7XD7Z9mQiyjLw1kXhcLKfJn3X9984Kpe9J7TUVk9RWaKVzxsj8KiVjBbKWdnttvT8K4xphuQzWQxVtjiBamoA9Ogw3zIp1xbghcVr20sXAk1t0uMlkAztUWfPM0NvbMEqYhioT+CeJglBKuTI1WCmqSovpaVSBsTua6bvIyQXiOtn7Qb7NpNEaK799PGRbdVFIqzBZJFhJxPFAyqMaUnh+n8QxQC7ARdjGkJxFf1senwTVbHPRKXsUGsKlHwfgVq/LXdWoKzsJ8mETPWyNuW3Y0kFyRMnmiUKeSC3yIzgeQUylR2ur4NUHRoxMUqVKGmQv9z7TQSlpFEKLoTIpvubo/A6qZaiGqqzk/coJDFrromi1zlg3Tv7qfIgsKbcpE2oGEWphcOFYYBqm69iMFho7DQKITlfec5EkD3Agms/jWNAWjK0SFE4cCGCIZkyCtimRPQKFyMKTgUTaKQojNop1ujXaDaCCgjBSyFtK14FGU0qSh0mkAEQo4UTX9kNC5XEIvqmzZwMT9oZELwg02lFxK3AVx9D7g1m+M7E6fQyGj8OouBmm/Ah74dhTLSaYGl3OzSSIP/2UrVJWYCFtpNGO0xfVlkRgpFP3BFcUTIJ5eV3Iy8vLwmeHo8bFS9PoY5EQ2a/5MZMHMLnDvppgR0Nh4MoHM5zLaRKcB3x45aEDCcJ0Rn448TCS77niiogecnEyXVGUpdFiiPbZBAHMFPk7HZFoPsxVbNUIm+s2JP0KW5vlq9Ygs+a98HRm/Ap5YhJ1LzwaRh5Rd0jPAeH76sIsi6pGECb9bYwurc2Xz27PZt7K5nISq4y97alGDFiZHvu4wZx1uwZ0Po1w1rQCY5b+F76Iixq7ayBlMe6yYkdt+dTJ8IqlCD5FSG3lKAAYvUx0JHAoYsJpF9tQlqhAbrTOVj//BuiVuPaTqKCXeNk+thTz2PXoQCcvffiilE9ZX+JNiOTeFTB8P+hC0ep+bjBSo4vYbAyBvPmLBDlOHc0LIsQav5vVmHrF2WCv06Loq+yCkWTXD2GdvPXMTiuuXxT2i5s0vxqjKLyeBD2QhNs+clIrztkwPuV1QI02VJtQnE5bC/ehbg+t92HDTdCr7owZsjdcFzVvSm54TRa9TAw8hPGiRZe1ZGJVOoBVcQCCsKeuEiwdedHQzX2QkE21eLmCPy+HNT77oFfHotL71CVQTCRcpdQX39Wu3rK+faD6ftAkQrEc27wORSXVUPO7OpAJ3fHkTPIAKNJJcGSy5W4DpGgDkGPDK87C67gtQjqrkNEvg2qlKzROxIV2PBXQ5iMLzq4ZWOtCfNdc3sl/IabkowNRWxEMbbGjyBsLEI/7XEMuP4oZCs1oB4DGk73RY3yKn3RvRZ5rT7xCf0lDUT3ocmWWgq3CqbTAgj/PU10Ol2aUZ/Ph3sfWoL9529H3DiqZQ2VKlqETtgBmzocU1ZOI6Z3djk9v5AQhS7xhZizKQ3wmcaLt53JnDiOhj1OUUelmI8E0rjPY7s2083Hi+u2YdP+gQjoi6DocrplxsyzwpvhtszoVii9Sv28Src2dKiEcTjpq2P91V4t5qtVqP1budAUt3hsOpalSzPKH/Bvbe0p3PTokJZNWxm1M8wJrUKT9VFholSieydhOhUZ0YNw2+Z34sGPST4T+j2Hba8u+O5XQQ0NDbhhzsDLaqpXogpvPtML/nCEGk4qN+hLE6F5us2M/MJC5FMOmzxrIw41L2zh0fVg2x/5C8aNLe16k0etlcZIyhdkbNvWMdc1r2/5M955vwnfeLmbZRTXYDNG0b8whifn3IZBdKHR+Rau420gA+LylW/go+NxXAoYib9M+yhwECyM+ZEJC+dNT4Il7c97sww8/tarIL6v5Js8XsQ3MdwfsiDZ2dlizGmBnZQFYL/gMb/jOR5zT8nAyDU4YxaHPjcJLEzr+843ef8D+3CjHJqHfLwAAAAASUVORK5CYII=";
          chatService
            .load(chat.id)
            .then((msgs) => {
              const messages = msgs.map((msg) => this.processMessage(msg));
              resolve({
                id: chat.id,
                specialist_id: chat.participants[0].id,
                specialist_name: "Покупатель",
                specialist_avatar: avatar,
                specialist_job_title: "Покупатель",
                specialist_presence: chat.participants[0].presence,
                status: chat.status,
                // TODO: Как получить общее кол-во сообщений?
                total_messages_count: messages.length,
                messages,
              });
            })
            .catch((ex) => reject(ex));
        } else {
          mainApi
            .getSpecialist(chat.participants[0].id)
            .then(({ data }) => {
              const specialist = new Specialist(data);
              chatService
                .load(chat.id)
                .then((msgs) => {
                  const messages = msgs.map((msg) => this.processMessage(msg));
                  resolve({
                    id: chat.id,
                    specialist_id: specialist.userId,
                    specialist_name: specialist.name,
                    specialist_avatar: specialist.avatarSrc,
                    specialist_job_title: specialist.jobTitle,
                    specialist_presence: chat.participants[0].presence,
                    status: chat.status,
                    total_messages_count: messages.length,
                    messages,
                  });
                })
                .catch((ex) => reject(ex));
            })
            .catch((ex) => reject(ex));
        }
      });
    });
    const settledPromises = await Promise.allSettled(promises);
    const chatsArray = settledPromises
      .filter((promise) => promise.status === "fulfilled")
      .map((promise) => promise.value);
    return chatsArray;
  }
  // TODO: Добавить уведомление об изменении статуса чата?
  async processChat(chat, notify = false) {
    const chatIndex = this.findChatIndex("id", chat.id);
    if (chatIndex > -1) {
      this._chats[chatIndex].status = chat.status;
    } else {
      const [processedChat] = await this.processChats([chat]);
      this._chats.push(processedChat);
      if (notify) {
        store._vm.$doNoty.info(
          i18n.t("chat.new_chat", { user: processedChat.specialist_name }),
          () => router.push(`/user/chat/${chat.id}`)
        );
      }
    }
  }

  @Action
  init() {
    if (this._initialized) {
      return Promise.resolve();
    }

    return chatService
      .connect()
      .then(() => {
        this.setEventListeners();
        this._isOnline = true;
        this._initialized = true;
      })
      .catch((ex) => console.log(ex));
  }

  @Action
  destroy() {
    chatService.reset();
    this._initialized = false;
    this._isOnline = false;
    this._chatsLoaded = false;
    this._chatsIsLoading = false;
    this._chats = [];
    this._currentChatId = null;
  }

  @Action
  async loadChats() {
    if (!this.online) return;

    this._chatsIsLoading = true;

    try {
      const chats = await chatService.loadChats();
      this._chats = await this.processChats(chats);
      this._chatsLoaded = true;
    } catch (ex) {
      store._vm.$doNoty.error(ex);
    }

    this._chatsIsLoading = false;
  }

  @Action
  openChat(id) {
    if (!this.online) return;

    this._currentChatId = id;
  }

  @Action
  async startChat(userId) {
    if (!this.online) return Promise.reject("You are offline");

    try {
      const chat = await chatService.start(userId);
      await this.processChat(chat);
      return Promise.resolve(chat.id);
    } catch (ex) {
      return Promise.reject(ex);
    }
  }

  @Action
  async sendMessage(message) {
    if (!this.online) return Promise.reject("You are offline");

    try {
      const msg = await chatService.send(this._currentChatId, message);
      const processedMessage = this.processMessage(msg);
      this._chats[this.chatIndex].messages.push(processedMessage);
      return Promise.resolve();
    } catch (ex) {
      return Promise.reject(ex);
    }
  }

  @Action
  leaveChat(chatId) {
    return chatService.leave(chatId);
  }
}

const cm = new ChatsModule({ store, name: "chats" });
cm.$watch(
  (module) => module._isOnline,
  (newValue) => {
    if (newValue) {
      cm.loadChats();
    }
  },
  {
    deep: false,
    immediate: true,
  }
);

export const chatsModule = cm;
