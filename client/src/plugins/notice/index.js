import Toastify from "toastify-js";
import "toastify-js/src/toastify.css";
import "./style.css";

export default {
  install(app) {
    const defaultText = "Что-то пошло не так";
    const types = {
      error: "error",
      warning: "warn",
      success: "success",
      info: "info",
    };

    const toaster = (type) => {
      return (msg, onClickFunction) => {
        // new Audio("/notification.mp3").play();
        const toast = Toastify({
          text: msg,
          className: type,
          duration: 5000,
          escapeMarkup: false,
          onClick() {
            if (onClickFunction) {
              onClickFunction();
            }
            toast.hideToast();
          },
        });
        toast.showToast();
      };
    };

    const Toast = {
      error: toaster("error"),
      warning: toaster("warn"),
      success: toaster("success"),
      info: toaster("info"),
    };

    const noticeObject = {};

    for (const key in types) {
      noticeObject[key] = (msg, cb) => {
        return new Promise((resolve) => {
          const msgText = msg || defaultText;
          Toast[key](msgText, cb);
          resolve(true);
        });
      };
    }

    app.prototype.$doNoty = noticeObject;
  },
};
