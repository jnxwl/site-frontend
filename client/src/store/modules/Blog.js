import { mainApi } from "@/services/api/main-api";
// models
import { BlogArticle } from "@/models/BlogArticle.js";

export default {
  namespaced: true,
  state: {
    blogs: {
      items: [],
      limit: 10,
      page: 1,
      total: 0,
      totalPages: 1,
    },
    blog: {},

    isBlogsLoading: false,
    isBlogLoading: false,
  },
  getters: {
    blogs: (state) => state.blogs.items.map(
      (item) => new BlogArticle(item)
    ),
    currentPage: (state) => state.blogs.page || 1,
    totalPages: (state) => state.blogs.totalPages || 1,
    blog: (state) => new BlogArticle(state.blog),
    isBlogsLoading: (state) => state.isBlogsLoading,
    isBlogLoading: (state) => state.isBlogLoading,
  },
  mutations: {
    setIsBlogsLoading(state, status) {
      state.isBlogsLoading = status;
    },
    setIsBlogLoading(state, status) {
      state.isBlogLoading = status;
    },
    setBlogs(state, blogs = {}) {
      state.blogs.total = blogs.count || 0;
      state.blogs.totalPages = state.blogs.total / state.blogs.limit || 1;
      state.blogs.page = blogs.page || 1;
      state.blogs.items = blogs.data || [];
    },
    setBlog(state, blog = {}) {
      state.blog = blog;
    },
  },
  actions: {
    async getBlogs({ state, commit }, page = 1) {
      try {
        commit("setIsBlogsLoading", true);
        const limit = state.blogs.limit;
        const { data } = await mainApi.getBlogs({
          limit,
          offset: (page - 1) * limit || 0,
        });

        commit("setBlogs", { ...data, page });
        commit("setIsBlogsLoading", false);
      } catch(error) {
        commit("setIsBlogsLoading", false);
        this._vm.$doNoty.error(error);
      }
    },
    async getBlog({ commit }, id) {
      try {
        commit("setIsBlogLoading", true);
        const { data } = await mainApi.getBlog(id);
        commit("setBlog", data);
        commit("setIsBlogLoading", false);
      } catch(error) {
        commit("setIsBlogLoading", false);
        this._vm.$doNoty.error(error);
      }
    },
  },
};
