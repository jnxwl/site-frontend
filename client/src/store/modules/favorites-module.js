import { VuexModule, Module, Action } from "vuex-class-modules";
import { accountModule } from "../index";

const SALONS_STORAGE_KEY = "favorite_salons";
const SERVICES_STORAGE_KEY = "favorite_services";
const SPECIALISTS_STORAGE_KEY = "favorite_specialists";

@Module({ generateMutationSetters: true })
class FavoritesModule extends VuexModule {
  favoriteSalons = [];
  favoriteServices = [];
  favoriteSpecialists = [];

  @Action init() {
    this.loadFavoriteSalons();
    this.loadFavoriteServices();
    this.loadFavoriteSpecialists();
  }

  @Action
  loadFavoriteSalons() {
    const userFavorites = accountModule.userInfo.favoriteSalons || [];
    if (userFavorites.length) {
      this.favoriteSalons = userFavorites;
      return;
    }

    const raw = localStorage.getItem(SALONS_STORAGE_KEY);
    try {
      this.favoriteSalons = JSON.parse(raw) || [];
    } catch (err) {
      this.favoriteSalons = [];
    }
  }

  @Action
  setFavoriteSalon({ salonId, isFavorite }) {
    if (isFavorite) {
      this.favoriteSalons = [...this.favoriteSalons, salonId];
    } else {
      this.favoriteSalons = this.favoriteSalons.filter(
        (item) => item !== salonId
      );
    }

    if (accountModule.userId) {
      accountModule.updateUserInfo({
        params: { favoriteSalons: this.favoriteSalons },
        successMessage: isFavorite
          ? "Добавлено в избранное"
          : "Удалено из избранного",
      });
    } else {
      localStorage.setItem(
        SALONS_STORAGE_KEY,
        JSON.stringify(this.favoriteSalons)
      );
    }
  }

  @Action
  loadFavoriteServices() {
    const userFavorites = accountModule.userInfo.favoriteServices || [];
    if (userFavorites.length) {
      this.favoriteServices = userFavorites;
      return;
    }

    const raw = localStorage.getItem(SERVICES_STORAGE_KEY);
    try {
      this.favoriteServices = JSON.parse(raw) || [];
    } catch (err) {
      this.favoriteServices = [];
    }
  }

  @Action
  setFavoriteService({ serviceId, specialistId, isFavorite }) {
    let specialistIndex = this.favoriteServices.findIndex(
      (item) => specialistId === item.specialistId
    );

    if (!~specialistIndex) {
      this.favoriteServices.push({
        specialistId,
        items: [],
      });
      specialistIndex = this.favoriteServices.length - 1;
    }
    const services = this.favoriteServices[specialistIndex].items;
    if (isFavorite) {
      this.favoriteServices[specialistIndex].items = [
        ...new Set([...services, serviceId]),
      ];
    } else {
      this.favoriteServices[specialistIndex].items = services.filter(
        (item) => item !== serviceId
      );
    }

    this.favoriteServices = this.favoriteServices.filter(
      (item) => item.items.length
    );

    if (accountModule.userId) {
      accountModule.updateUserInfo({
        params: { favoriteServices: this.favoriteServices },
        successMessage: isFavorite
          ? "Добавлено в избранное"
          : "Удалено из избранного",
      });
    } else {
      localStorage.setItem(
        SERVICES_STORAGE_KEY,
        JSON.stringify(this.favoriteServices)
      );
    }
  }

  @Action
  loadFavoriteSpecialists() {
    const userFavorites = accountModule.userInfo.favoriteSpecialists || [];
    if (userFavorites.length) {
      this.favoriteSpecialists = userFavorites;
      return;
    }

    const raw = localStorage.getItem(SPECIALISTS_STORAGE_KEY);
    try {
      this.favoriteSpecialists = JSON.parse(raw) || [];
    } catch (err) {
      this.favoriteSpecialists = [];
    }
  }

  @Action
  setFavoriteSpecialist({ specialistId, isFavorite }) {
    if (isFavorite) {
      this.favoriteSpecialists = [...this.favoriteSpecialists, specialistId];
    } else {
      this.favoriteSpecialists = this.favoriteSpecialists.filter(
        (item) => item !== specialistId
      );
    }

    if (accountModule.userId) {
      accountModule.updateUserInfo({
        params: { favoriteSpecialists: this.favoriteSpecialists },
        successMessage: isFavorite
          ? "Добавлено в избранное"
          : "Удалено из избранного",
      });
    } else {
      localStorage.setItem(
        SERVICES_STORAGE_KEY,
        JSON.stringify(this.favoriteSpecialists)
      );
    }
  }
}

export default FavoritesModule;
