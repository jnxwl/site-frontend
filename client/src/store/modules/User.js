import Cookie from "js-cookie";
import { authApi } from "@/services/api/auth-api";

export default {
  namespaced: true,

  state: {
    user: null,
  },
  mutations: {
    setUser(state, user) {
      const oldKeys = Object.keys(user.userinfo);
      const keys = Object.keys(user.userinfo).map((key) => {
        let keyArr = key.split("_");
        if (keyArr.length === 1) return keyArr[0];
        for (let i = 1; i < keyArr.length; i++) {
          keyArr[i] = keyArr[i].split("");
          keyArr[i][0] = keyArr[i][0].toUpperCase();
          keyArr[i] = keyArr[i].join("");
        }
        return keyArr.join("");
      });
      keys.forEach((key, i) => {
        if (key !== oldKeys[i]) {
          user.userinfo[key] = user.userinfo[oldKeys[i]];
          delete user.userinfo[oldKeys[i]];
        }
      });
      state.user = user;
    },
    updateUserInfo(state, userinfo) {
      state.user.userinfo = { ...state.user.userinfo, ...userinfo };
    },
    setUserNull(state) {
      state.user = null;
    },
  },
  actions: {
    getUserInfo({ commit }, params) {
      return authApi
        .getUserInfo(params)
        .then((response) => {
          if (response.data.ok) {
            commit("setUser", response.data);
          } else {
            commit("setUserNull");
          }
          commit(
            "Auth/changeAuthStatus",
            { isAuthorized: response.data.ok },
            { root: true }
          );
        })
        .catch((error) => {
          this._vm.$doNoty.error(error);
        });
    },
    updateUserInfo({ state, commit }, params) {
      return authApi
        .updateUserInfo({
          accessToken: Cookie.get("accessToken"),
          userinfo: { ...state.user.userinfo, ...params },
        })
        .then((response) => {
          if (response.data.ok) {
            commit("updateUserInfo", params);
          } else {
            this._vm.$doNoty.error(response.data.msg);
          }
          return response.data;
        })
        .catch((error) => {
          this._vm.$doNoty.error(error);
        });
    },
  },
  getters: {
    user: (state) => state.user,
  },
};
