import { mainApi } from "@/services/api/main-api";
import { Salon } from "@/models/Salon";
import { MapSalon } from "@/models/MapSalon";
import { Category } from "@/models/Category";
import { Service } from "@/models/Service";
import { camelCase } from "lodash";

export default {
  namespaced: true,
  state: {
    isSalonsLoading: false,
    isNextSalonsLoading: false,
    salons: [],
    salonsCount: 0,
    mapSalons: [],
    mapSalonsCount: 0,
    mapMarks: [],
    salonsOffset: 0,
    services: [],
    salonTypes: [],
    serviceCategories: [],
    bodyParts: [],
    genderOptions: [
      {
        id: "М",
        title: "Мужской",
      },
      {
        id: "Ж",
        title: "Женский",
      },
      {
        id: "У",
        title: "Унисекс",
      },
    ],
    brands: [
      { id: 1, title: "Бренд 1" },
      { id: 2, title: "Бренд 2" },
      { id: 3, title: "Бренд 3" },
      { id: 4, title: "Бренд 4" },
    ],
    reviewOptions: [
      { id: "positive", title: "Только положительные" },
      { id: "mixed", title: "Смешанные" },
      { id: "negative", title: "Только негативные" },
      { id: "all", title: "Все" },
    ],
    bookingOptions: [
      { id: "free-cancel", title: "Бесплатная отмена бронирования" },
      { id: "no-credit", title: "Бронирование без кредитной карты" },
      { id: "no-prepay", title: "Без предоплаты" },
    ],
  },
  mutations: {
    setServices(state, services) {
      state.services = [...services];
    },
    setSalonTypes(state, salonTypes) {
      state.salonTypes = [...salonTypes];
    },
    setServiceCategories(state, serviceCategories) {
      state.serviceCategories = [...serviceCategories];
    },
    setSalons(state, salons) {
      state.salons = [...salons];
    },
    setSalonsCount(state, count) {
      state.salonsCount = count;
    },
    setMapSalons(state, salons) {
      state.mapSalons = [...salons];
    },
    setMapSalonsCount(state, count) {
      state.mapSalonsCount = count;
    },
    setMapMarks(state, marks) {
      state.mapMarks = [...marks];
    },
    setSalonsOffset(state, offset) {
      state.salonsOffset = offset;
    },
    pushSalons(state, salons) {
      state.salons.push(...salons);
    },
    setBodyParts(state, parts) {
      state.bodyParts = [...parts];
    },
    setBrands(state, brands) {
      state.brands = [...brands];
    },
    setSalonsLoading(state, isLoading) {
      state.isSalonsLoading = isLoading;
    },
    setNextSalonsLoading(state, isLoading) {
      state.isNextSalonsLoading = isLoading;
    },
  },

  actions: {
    getServices({ commit }, search) {
      return mainApi
        .getServices({ title: search })
        .then((response) => {
          commit("setServices", response.data.data);
          return response.data.data;
        })
        .catch((error) => {
          this._vm.$doNoty.error(error);
        });
    },

    getSalonTypes({ commit }) {
      return mainApi
        .getSalonTypes()
        .then((response) => {
          commit("setSalonTypes", response.data.data);
          return response.data.data;
        })
        .catch((error) => {
          this._vm.$doNoty.error(error);
        });
    },

    getServiceCategories({ commit }) {
      return mainApi
        .getServiceCategories({
          is_active: 1,
          "data[showInHeader]": true,
        })
        .then((response) => {
          commit("setServiceCategories", response.data.data);
          return response.data.data;
        })
        .catch((error) => {
          this._vm.$doNoty.error(error);
        });
    },

    getBodyParts({ commit }) {
      return mainApi
        .getBodyParts()
        .then((response) => {
          commit("setBodyParts", response.data.data);
          return response.data.data;
        })
        .catch((error) => {
          this._vm.$doNoty.error(error);
        });
    },

    async getSalons({ commit, getters }, query) {
      commit("setSalonsOffset", 0);
      const { data } = await mainApi.searchSalons(
        composeSalonsQuery(query, getters)
      );
      const { data: stats } = await mainApi.getReviewsStatistics({
        salon_id: data.data.map((item) => item.id),
      });

      commit(
        "setSalons",
        data.data.map((item) => ({
          ...(stats.find((s) => s.id === item.id)?.statistic || {}),
          ...item,
        }))
      );
      commit("setSalonsCount", data.count);
      commit("setSalonsOffset", getters.salonsOffset + 1);
    },

    async getMapSalons({ commit }, query) {
      const { data } = await mainApi.searchSalons(
        composeSalonsQuery(query, { offset: 0 })
      );
      const { data: stats } = await mainApi.getReviewsStatistics({
        salon_id: data.data.map((item) => item.id),
      });

      commit(
        "setMapSalons",
        data.data.map((item) => ({
          ...(stats.find((s) => s.id === item.id)?.statistic || {}),
          ...item,
        }))
      );
      commit("setMapSalonsCount", data.count);
    },

    async getMapMarks({ state, commit }, query) {
      const { data: { count } } = await mainApi.searchMapSalons(
        composeSalonsQuery(query, { limit: 0 })
      );
      commit("setMapMarks", []);
      const limit = +process.env.VUE_APP_MAP_SALONS_LIMIT || 100;
      let mapMarksCount = count;
      let salonsOffset = 0;

      while (mapMarksCount > 0) {
        const { data: { data } } = await mainApi.searchMapSalons(
          composeSalonsQuery(query, {
            salonsOffset, limit,
          })
        );
        mapMarksCount -= limit;
        commit("setMapMarks", [
          ...data,
          ...state.mapMarks,
        ]);
        salonsOffset++;
      }
    },

    async getNextSalons({ commit, getters }, query) {
      commit("setNextSalonsLoading", true);

      const { data } = await mainApi.searchSalons(
        composeSalonsQuery(query, getters)
      );
      const { data: stats } = await mainApi.getReviewsStatistics({
        salon_id: data.data.map((item) => item.id),
      });

      commit(
        "pushSalons",
        data.data.map((item) => ({
          ...(stats.find((s) => s.id === item.id)?.statistic || {}),
          ...item,
        }))
      );
      commit("setSalonsOffset", getters.salonsOffset + 1);
      commit("setNextSalonsLoading", false);
    },

    setSalonsLoading({ commit }, isLoading) {
      commit("setSalonsLoading", isLoading);
    },
  },

  getters: {
    isSalonsLoading: (state) => state.isSalonsLoading,
    isNextSalonsLoading: (state) => state.isNextSalonsLoading,
    salons: (state) => state.salons.map((item) => new Salon(item)),
    salonById: (state) => (id) => {
      const salon = state.salons.find((item) => item.id === id);
      return salon ? new Salon(salon) : null;
    },
    salonsCount: (state) => state.salonsCount,
    mapSalons: (state) => state.mapSalons.map((item) => new Salon(item)),
    mapMarks: (state) => state.mapMarks.map((item) => new MapSalon(item)),
    mapSalonsCount: (state) => state.mapSalonsCount,
    salonsOffset: (state) => state.salonsOffset,
    services: (state) => state.services.map((item) => new Service(item)),
    salonTypes: (state) => state.salonTypes,
    serviceCategories: (state) =>
      state.serviceCategories.map((item) => new Category(item)),
    bodyParts: (state) => state.bodyParts,
    genderOptions: (state) => state.genderOptions,
    brands: (state) => state.brands,
    reviewOptions: (state) => state.reviewOptions,
    bookingOptions: (state) => state.bookingOptions,
  },
};

function composeSalonsQuery(query, getters) {
  const finalQuery = {
    is_confirmed: 1,
    limit: 6,
    limit_salon_service: 3,
    offset_salon_service: 0,
    offset: getters.salonsOffset || 0,
    price_from: query.priceFrom,
    price_to: query.priceTo,
    service: query.service,
    gender: query.gender,
    latitude: query.latitude,
    longitude: query.longitude,
    radius: query.radius,
  };

  if (typeof getters.limit !== "undefined") {
    finalQuery.limit = getters.limit;
  }

  const arrayTypes = ["category_id", "salon_type_id", "body_part_id"];

  arrayTypes.forEach((item) => {
    const queryKey = camelCase(item);
    if (typeof query[queryKey] === "string") {
      query[queryKey].split(",").forEach((value, index) => {
        finalQuery[`${item}[${index}]`] = value;
      });
    } else {
      finalQuery[item] = query[queryKey];
    }
  });

  return finalQuery;
}
