import { COUNTRY_FLAGS } from "@/helpers/country-flags";

export default {
  namespaced: true,
  state() {
    return {
      activeRegionPopup: false,
      regions: COUNTRY_FLAGS,
      activeRegion: {
        region: "ru",
        name: "Россия",
        number: "",
      },
    };
  },
  mutations: {
    openPopup(state) {
      state.activeRegionPopup = state.activeRegionPopup !== true;
    },
    chooseRegion(state, region) {
      state.activeRegion = region;
      state.activeRegionPopup = false;
    },
  },
  actions: {},
  getters: {
    regions(state) {
      return state.regions;
    },
    activeRegionPopup(state) {
      return state.activeRegionPopup;
    },
    activeRegion(state) {
      return state.activeRegion;
    },
  },
};
