import { authApi } from "@/services/api/auth-api";
import { mainApi } from "@/services/api/main-api";
import i18n from "@/plugins/i18n";
import Cookie from "js-cookie";

export default {
  namespaced: true,

  state: {
    mailing: [],
    isAuthorized: false,
    remember: false,
    activeError: i18n.tc("auth.requiredField"),
  },

  getters: {
    mailing: (state) => state.mailing,
    isAuthorized: (state) => state.isAuthorized,
    errors: (state) => state.activeError,
  },

  mutations: {
    changeAuthStatus(state, { isAuthorized }) {
      state.isAuthorized = isAuthorized;
    },

    setRemember(state, remember) {
      state.remember = remember;
    },
  },

  actions: {
    setTokens({ state, commit }, tokens) {
      const expire = state.remember
        ? (1 / 1440) * process.env.VUE_APP_LIFE_TIME_TOKEN_WITH_REMEBER
        : (1 / 1440) * process.env.VUE_APP_LIFE_TIME_TOKEN;
      Cookie.set("accessToken", tokens.accessToken, {
        expires: expire,
        domain: location.hostname,
      });
      Cookie.set("refreshToken", tokens.refreshToken, {
        expires: expire,
        domain: location.hostname,
      });
      commit("changeAuthStatus", { isAuthorized: true });
    },

    removeTokens({ commit }) {
      commit("changeAuthStatus", { isAuthorized: false });
      Cookie.remove("accessToken");
      Cookie.remove("refreshToken");
    },

    async getRecaptchaSiteKey() {
      const response = await authApi.getRecaptchaSiteKey();
      return response.data;
    },

    async sendConfirmCode(_, { email, phone }) {
      const body = {
        email: email,
        phone: phone,
      };
      await authApi.sendConfirmCode(body);
    },

    async login({ dispatch }, params) {
      try {
        const response = await authApi.login(params);
        if (!response.data.ok) throw response.data;

        await dispatch("setTokens", response.data);
        await this._vm.$doNoty.success(i18n.tc("auth.success_auth"));

        return response;
      } catch (error) {
        this._vm.$doNoty.error(error.msg);
      }
    },

    async registerCompany(_, params) {
      try {
        const response = await mainApi.registerCompany(params);
        if (!response.data.id) throw response;

        await this._vm.$doNoty.success(
          i18n.tc("auth.success_register_company")
        );

        return response;
      } catch (error) {
        this._vm.$doNoty.error(error);
      }
    },

    async registerBusiness(_, params) {
      try {
        const response = await authApi.registerBusiness(params);
        if (response && !response.data.ok) throw response.data;
        return response;
      } catch (error) {
        this._vm.$doNoty.error(error.msg);
      }
    },

    resetPassword(_, params) {
      return authApi
        .resetPassword(params)
        .then((response) => {
          if (response.data.ok) {
            this._vm.$doNoty.success(i18n.tc("auth.passChanged"));
          } else {
            this._vm.$doNoty.error(response.data.msg);
          }
          return response;
        })
        .catch((error) => {
          this._vm.$doNoty.error(error);
        });
    },

    async registerClient(_, params) {
      try {
        const response = await authApi.registerClient(params);
        if (!response.data.ok) throw response.data;

        await this._vm.$doNoty.success(i18n.tc("auth.succees_register"));
        return response;
      } catch (error) {
        this._vm.$doNoty.error(error.msg);
      }
    },

    getOAuthLink(_, params) {
      return authApi
        .getOAuthLink(params)
        .then((response) => {
          if (response.data) {
            return response;
          }
        })
        .catch((error) => {
          console.error(error);
        });
    },

    async getInfoFromInn(_, params) {
      try {
        const response = await authApi.getInfoFromInn(params);
        if (response.data) return response;
      } catch (err) {
        console.error(err);
      }
    },

    async oauthLogin(_, params) {
      try {
        const response = await authApi.oauthLogin(params);
        if (response.data) {
          return response;
        }
      } catch (error) {
        console.error(error);
      }
    },
  },
};
