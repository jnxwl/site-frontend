import { VuexModule, Module, Mutation, Action } from "vuex-class-modules";
import { Order } from "@/models/Order";
import { mainApi } from "@/services/api/main-api";
import { accountModule } from "../index";
import store from "@/store";

@Module({ generateMutationSetters: true })
class ReviewModule extends VuexModule {
  order = new Order();

  @Mutation
  setOrder(order) {
    this.order = order;
  }

  @Action
  async createReview({ rating, message }) {
    try {
      const { data } = await mainApi.createReview({
        user_id: accountModule.userId,
        salon_id: this.order.salon.id,
        rating,
        body: message,
        data: {
          name: accountModule.fullName,
          avatar_src: accountModule.userInfo.avatar || "",
        },
      });
      return data;
    } catch (err) {
      store._vm.$doNoty.error(err);
    }
  }
}

export default ReviewModule;
