import { notyApi } from "@/services/api/noty-api";
import { VuexModule, Module, Action } from "vuex-class-modules";
import store from "@/store";
import { WS } from "@/services/ws";
import Cookie from "js-cookie";

@Module({ generateMutationSetters: true })
class BellModule extends VuexModule {
  unreadCount = 0;
  isInitted = false;
  @Action init() {
    if (this.isInitted) return;
    this._ws = new WS(
      process.env.VUE_APP_NOTIFICATIONS_URI ||
        location.origin.replace("http", "ws") + "/noty/notifications",
      Cookie.get("accessToken")
    );
    this._ws.connect();
    this._ws.addEventListener("notification:new_inbox_message", () =>
      this.loadUnreadCount(true)
    );
    this.isInitted = true;
  }

  @Action
  async loadUnreadCount(showToast = false) {
    const { data } = await notyApi.getUnreadMessagesCount();
    if (data.count !== this.unreadCount && showToast) {
      store._vm.$doNoty.info(
        `Вам поступило новое уведомление! У вас ${data.count} непрочитанных уведомлений`
      );
    }
    this.unreadCount = Number(data.count || 0);
  }
}

export default BellModule;
