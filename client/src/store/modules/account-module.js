import { VuexModule, Module, Action } from "vuex-class-modules";

import { authApi } from "@/services/api/auth-api";
import i18n from "@/plugins/i18n";
import store from "@/store";

import { USER_STATUSES } from "@/constants/user-statuses";
import { getUserId } from "@/helpers/tokens";
import { getCrmRedirectUrl } from "@/helpers/links";

import Cookie from "js-cookie";
import { paymentsApi } from "@/services/api/payments-api";
import { CashbackLevel } from "@/models/CashbackLevel";
import { favoritesModule } from "../index";

const COOKIE_CITY_KEY = "city_name";
const COOKIE_LONGITUDE_KEY = "city_longitude";
const COOKIE_LATITUDE_KEY = "city_latitude";

@Module({ generateMutationSetters: true })
class AccountModule extends VuexModule {
  userId = "";
  email = "";
  phone = "";
  roles = [];
  userInfo = {};

  balance = 0;
  cashback = 0;
  spentAmount = 0;
  status = USER_STATUSES.classic;
  cashbackLevels = [];

  city = Cookie.get(COOKIE_CITY_KEY) || "";
  latitude = Cookie.get(COOKIE_LATITUDE_KEY) || "55.76";
  longitude = Cookie.get(COOKIE_LONGITUDE_KEY) || "37.64";

  get fullName() {
    return [this.userInfo.firstName || "", this.userInfo.lastName || ""]
      .filter((val) => val)
      .join(" ");
  }

  get avatar() {
    return (
      this.userInfo.avatar ||
      require("@/assets/img/icon/profile-icon/user-account.svg")
    );
  }

  get currentCashbackLevel() {
    return (
      this.cashbackLevels.find((item) => item.title === this.status) ||
      new CashbackLevel()
    );
  }

  get nextCashbackLevel() {
    const index = this.cashbackLevels.indexOf(this.currentCashbackLevel);
    return this.cashbackLevels[index + 1] || new CashbackLevel();
  }

  get discountType() {
    return this.userInfo.discountType || "";
  }

  // TODO:
  get referralLink() {
    return `${process.env.VUE_APP_BASE_URL || ""}/register/?refid=11111`;
  }

  get isAdmin() {
    return (
      this.roles.includes("admin-company") || this.roles.includes("super-admin")
    );
  }

  get isSpecialist() {
    return this.roles.includes("specialist");
  }

  get crmLink() {
    return getCrmRedirectUrl();
  }

  get favoriteSalons() {
    return this.userInfo.favoriteSalons;
  }

  get favoriteSpecialists() {
    return this.userInfo.favoriteSpecialists;
  }

  get favoriteServices() {
    return this.userInfo.favoriteServices;
  }

  @Action
  async loadUserInfo() {
    try {
      const accessToken = Cookie.get("accessToken");
      if (!accessToken) return;

      const { sub } = await getUserId(accessToken);
      this.userId = sub;

      const [
        { data: user },
        { data: balance },
        { data: status },
        { data: cashback },
      ] = await Promise.all([
        authApi.getUserInfo({ accessToken }),
        paymentsApi.getBalance({
          access_token: accessToken,
          currency: "RUB",
        }),
        paymentsApi.getAccountStatus({
          access_token: accessToken,
          currency: "RUB",
        }),
        paymentsApi.getCashbackSettings(),
      ]);

      this.balance = Number(balance.amount) || 0;
      this.cashback = Number(balance.cashback) || 0;
      this.status = status.status || USER_STATUSES.classic;
      this.cashbackLevels = (cashback.levels || []).map(
        (item) => new CashbackLevel(item)
      );

      this.email = user.email || "";
      this.phone = user.phone || "";
      this.roles = user.roles || [];
      this.userInfo = user.userinfo || {};

      const { data: spent } = await paymentsApi.getSpentAmount({
        access_token: accessToken,
        currency: "RUB",
        start_date:
          new Date(Date.now() - Number(cashback.check_period * 1000 || 0))
            .toISOString()
            .split(".")[0] + "Z",
      });
      this.spentAmount = Number(spent.amount || 0);

      favoritesModule.init();
    } catch (err) {
      console.error(err);
      store._vm.$doNoty.error(err);
    }
  }

  @Action
  async updateUserInfo({
    params,
    successMessage = "Данные успешно обновлены",
  }) {
    try {
      const updatedInfo = { ...this.userInfo, ...params };
      const { data } = await authApi.updateUserInfo({
        accessToken: Cookie.get("accessToken"),
        userinfo: updatedInfo,
      });

      if (data.ok) {
        this.userInfo = updatedInfo;
        store._vm.$doNoty.success(successMessage);
      } else {
        store._vm.$doNoty.error(data.msg);
      }
    } catch (err) {
      console.error(err);
      store._vm.$doNoty.error(err);
    }
  }

  @Action
  async updateLogin(params) {
    try {
      if (!params.email && !params.phone)
        return store._vm.$doNoty.error(i18n.t("auth.requiredField"));
      if (params.email && !params.emailConfirmCode)
        return store._vm.$doNoty.error(i18n.t("auth.confirmEmail"));
      if (params.phone && !params.phoneConfirmCode)
        return store._vm.$doNoty.error(i18n.t("auth.confirmPhone"));

      const { data } = await authApi.changeLogin(params);

      if (data.ok) {
        if (params.email) this.email = params.email;
        if (params.phone) this.phone = params.phone;
        store._vm.$doNoty.success(i18n.t("auth.dataChanged"));
      } else {
        store._vm.$doNoty.error(data.msg);
      }
    } catch (err) {
      console.error(err);
      store._vm.$doNoty.error(err);
    }
  }

  @Action
  changeCity({ cityName, countryCode, longitude, latitude } = {}) {
    this.city = countryCode || cityName || "Москва";
    if (/Moscow/i.test(this.city)) this.city = "Москва";
    this.latitude = latitude || "55.76";
    this.longitude = longitude || "37.64";

    Cookie.set(COOKIE_CITY_KEY, this.city, {
      expires: 30,
    });
    Cookie.set(COOKIE_LATITUDE_KEY, this.latitude, {
      expires: 30,
    });
    Cookie.set(COOKIE_LONGITUDE_KEY, this.longitude, {
      expires: 30,
    });
  }

  @Action
  signOut() {
    this.userId = "";
    this.email = "";
    this.phone = "";
    this.roles = [];
    this.userInfo = {};

    this.balance = 0;
    this.cashback = 0;
    this.status = USER_STATUSES.classic;
  }
}

export default AccountModule;
