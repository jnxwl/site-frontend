export default {
  namespaced: true,
  state() {
    return {
      registrationForm: {
        name: "",
        surname: "",
        password: "",
        recaptchaToken: "",
        phone: "",
        email: "",
      },
      rep_password: "",
    };
  },
  mutations: {
    setRecaptchaToken(state, token) {
      state.registrationForm.recaptchaToken = token;
    },
    setPassword(state, password) {
      state.registrationForm.password = password;
    },
    setName(state, name) {
      state.registrationForm.name = name;
    },
    setSurname(state, surname) {
      state.registrationForm.surname = surname;
    },
    setRepPassword(state, rep_password) {
      state.rep_password = rep_password;
    },
    setPhone(state, phone) {
      state.registrationForm.phone = phone;
    },
    setEmail(state, email) {
      state.registrationForm.email = email;
    },
  },
  actions: {},
  getters: {
    registrationForm(state) {
      return state.registrationForm;
    },
    rep_password(state) {
      return state.rep_password;
    },
  },
};
