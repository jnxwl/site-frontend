export default {
  namespaced: true,
  state() {
    return {
      registrationClinicForm: {
        company_name: "",
        inn: "",
        name: "",
        password: "",
        telephone: "",
        email: "",
        recaptchaToken: "",
        work_from_home: "",
      },
      validation: {
        email: -1,
        telephone: -1,
      },
      rep_password: "",
    };
  },
  mutations: {
    setRecaptchaToken(state, token) {
      state.registrationClinicForm.recaptchaToken = token;
    },
    setInnToken(state, inn) {
      state.registrationClinicForm.inn = inn;
    },
    setwork_from_homeToken(state, work_from_home) {
      state.registrationClinicForm.work_from_home = work_from_home;
    },
    setPassword(state, password) {
      state.registrationClinicForm.password = password;
    },
    setName(state, name) {
      state.registrationClinicForm.name = name;
    },
    setCompanyName(state, company_name) {
      state.registrationClinicForm.company_name = company_name;
    },
    setRepPassword(state, rep_password) {
      state.rep_password = rep_password;
    },
    setEmail(state, email) {
      state.registrationClinicForm.email = email;
    },
    setTelephone(state, telephone) {
      state.registrationClinicForm.telephone = telephone;
    },
    sendEmail(state, email) {
      state.validation.email = email;
    },
    sendTelephone(state, telephone) {
      state.validation.telephone = telephone;
    },
  },
  actions: {},
  getters: {
    registrationClinicForm(state) {
      return state.registrationClinicForm;
    },
    rep_password(state) {
      return state.rep_password;
    },
    validation(state) {
      return state.validation;
    },
  },
};
