import { uniqBy } from "lodash";

import { mainApi } from "@/services/api/main-api";

import { favoritesModule } from "../";

import { Specialist } from "@/models/Specialist";
import { SpecialistService } from "@/models/SpecialistService";
import { SearchService } from "@/models/SearchService";
import { Category } from "@/models/Category";
import { Salon } from "@/models/Salon";
import { GalleryImage } from "@/models/GalleryImage";

export default {
  namespaced: true,
  state: {
    isCategoriesLoading: false,
    isSpecialistsLoading: false,
    isNextSpecialistsLoading: false,
    isSpecialistLoading: false,
    isSpecialistGalleryLoading: false,
    specialistsCategories: [],
    specialists: [],
    specialistsCount: 0,
    specialistsOffset: 0,
    specialistsPageTitle: "Специалисты",
    specialist: {},
    services: [],
    salons: [],
    gallery: [],
  },
  mutations: {
    setCategoriesLoading(state, isLoading) {
      state.isCategoriesLoading = isLoading;
    },
    setSpecialistsLoading(state, isLoading) {
      state.isSpecialistsLoading = isLoading;
    },
    setNextSpecialistsLoading(state, isLoading) {
      state.isNextSpecialistsLoading = isLoading;
    },
    setSpecialistLoading(state, isLoading) {
      state.isSpecialistLoading = isLoading;
    },
    setSpecialistPortfolioLoading(state, isLoading) {
      state.isSpecialistGalleryLoading = isLoading;
    },
    setSpecialistsCategories(state, categories) {
      state.specialistsCategories = categories;
    },
    setSpecialists(state, specialists) {
      state.specialists = specialists;
    },
    pushSpecialists(state, specialists) {
      state.specialists.push(...specialists);
    },
    setSpecialistsCount(state, count) {
      state.specialistsCount = count;
    },
    setSpecialistsOffset(state, offset) {
      state.specialistsOffset = offset;
    },
    setSpecialistsPageTitle(state, title) {
      state.specialistsPageTitle = title;
    },
    setSpecialist(state, specialist) {
      state.specialist = specialist;
    },
    setSpecialistServices(state, services) {
      state.services = services;
    },
    setSpecialistSalons(state, salons) {
      state.salons = salons;
    },
    setSpecialistGallery(state, gallery) {
      state.gallery = gallery;
    },
    setFavoriteSpecialistStatus(state, status) {
      state.isFavorite = status;
    },
  },

  actions: {
    async getSpecialistsCategories({ commit }) {
      commit("setCategoriesLoading", true);
      try {
        const { data: categories } = await mainApi.getServiceCategories({
          is_active: 1,
        });
        commit("setSpecialistsCategories", categories.data);
      } catch (error) {
        this._vm.$doNoty.error(error);
      }
      commit("setCategoriesLoading", false);
    },

    async getSpecialists({ state, commit, dispatch }, categoryId) {
      commit("setSpecialistsLoading", true);
      try {
        commit("setSpecialistsCount", 0);
        commit("setSpecialistsOffset", 0);

        await dispatch("getSpecialistsCategories");

        const { data: specialists } = await mainApi.getSpecialists({
          category_id: categoryId,
          limit: 8,
          offset: state.specialistsOffset,
        });

        commit("setSpecialists", specialists.data);
        commit("setSpecialistsOffset", state.specialistsOffset + 1);
        commit("setSpecialistsCount", specialists.count);
        dispatch("updateSpecialistsCategoryName", categoryId);
      } catch (error) {
        this._vm.$doNoty.error(error);
      }
      commit("setSpecialistsLoading", false);
    },

    async getNextSpecialists({ state, commit }, categoryId) {
      commit("setNextSpecialistsLoading", true);
      try {
        const { data: specialists } = await mainApi.getSpecialists({
          category_id: categoryId,
          limit: 8,
          offset: state.specialistsOffset,
        });
        commit("pushSpecialists", specialists.data);
        commit("setSpecialistsOffset", state.specialists.length);
      } catch (error) {
        this._vm.$doNoty.error(error);
      }
      commit("setNextSpecialistsLoading", false);
    },

    async clearSpecialists({ commit, dispatch }) {
      commit("setSpecialistsLoading", true);
      commit("setSpecialistsCount", 0);
      commit("setSpecialistsOffset", 0);
      dispatch("updateSpecialistsCategoryName", -1);
      commit("setSpecialists", []);
    },

    updateSpecialistsCategoryName({ commit, state }, categoryId) {
      const { title } =
        (state.specialistsCategories || []).find(
          (item) => +item.id === +categoryId
        ) || {};
      const categoryName = title
        ? `Специалисты в категории "${title}"`
        : "Специалисты";
      commit("setSpecialistsPageTitle", categoryName);
    },

    async getSpecialist({ commit }, id) {
      try {
        commit("setSpecialistLoading", true);

        const { data: specialist } = await mainApi.getSpecialist(id);
        const { data: services } = await mainApi.searchServices({
          specialist_id: [specialist.id],
        });

        favoritesModule.loadFavoriteSpecialists();
        commit("setSpecialist", specialist);
        commit("setSpecialistServices", services.services);
        commit("setSpecialistLoading", false);
      } catch (error) {
        this._vm.$doNoty.error(error);
      }
    },

    async getSpecialistPortfolio({ state, commit }) {
      try {
        commit("setSpecialistPortfolioLoading", true);

        const salonIds = [
          ...new Set(state.services.map((item) => item.salon_id)),
        ];
        if (!salonIds?.length) return;

        const [{ data: salons }, { data: gallery }] = await Promise.all([
          mainApi.getSalons({
            "id[]": salonIds,
          }),
          mainApi.getGallery(11),
        ]);

        commit("setSpecialistSalons", salons.data);
        commit("setSpecialistGallery", gallery.images);
        commit("setSpecialistPortfolioLoading", false);
      } catch (error) {
        this._vm.$doNoty.error(error);
      }
    },

    async toggleFavoriteSpecialist({ state, getters }) {
      try {
        await favoritesModule.setFavoriteSpecialist({
          specialistId: state.specialist.id,
          isFavorite: !getters.isFavorite,
        });
      } catch (error) {
        this._vm.$doNoty.error(error);
      }
    },
  },

  getters: {
    categories: (state) =>
      state.specialistsCategories.map((item) => new Category(item)),
    specialistCategories: (state) =>
      uniqBy(
        state.services.map((item) => new Category(item.specialist_service.service.category)),
        "id"
      ),
    getSpecialistCategoryMinPrice: (state, getters) => (id) => {
      const services = getters.services.filter(
        (service) => service.specialistService.categoryId === id
      );
      return Math.min(...services.map((service) => service.specialistService.price));
    },
    getSpecialistCategoryMinPriceWithDiscount: (state, getters) => (id) => {
      const services = getters.services.filter(
        (service) => service.specialistService.categoryId === id
      );
      return Math.min(...services.map((service) => service.specialistService.priceWithDiscount));
    },
    services: (state) =>
      state.services.map((item) => new SearchService(item)),
    specialists: (state) =>
      (state.specialists || []).map((item) => new Specialist(item)) || [],
    specialist: (state) =>
      new Specialist({
        ...state.specialist,
      }),
    isFavorite: (state, getters) =>
      !!favoritesModule.favoriteSpecialists.find(
        (item) => item === getters.specialist.id
      ),
    salons: (state) =>
      (state.salons || []).map((item) => new Salon(item)) || [],
    gallery: (state) =>
      (state.gallery || []).map((item) => new GalleryImage(item)) || [],
  },
};
