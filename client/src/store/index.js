import Vue from "vue";
import Vuex from "vuex";

import Popup from "./modules/Popup";
import Registration from "./modules/Registration";
import RegistrationClinic from "./modules/RegistrationObject";
import Auth from "./modules/Authorize";
import Search from "./modules/Search";
import User from "./modules/User";
import Specialist from "./modules/Specialist";
import Blog from "./modules/Blog";

import SalonModule from "./modules/salon-module";
import OrderModule from "./modules/order-module";
import AccountModule from "./modules/account-module";
import FavoritesModule from "./modules/favorites-module";
import PaymentModule from "./modules/payment-module";
import ReviewModule from "./modules/review-module";
import BellModule from "./modules/bell-module";

Vue.use(Vuex);

const store = new Vuex.Store({
  modules: {
    Auth,
    Search,
    User,
    Registration,
    Popup,
    RegistrationClinic,
    Specialist,
    Blog,
  },
});

export const salonModule = new SalonModule({ store, name: "salon" });
export const orderModule = new OrderModule({ store, name: "order" });
export const accountModule = new AccountModule({ store, name: "account" });
export const bellModule = new BellModule({ store, name: "bell" });
export const paymentModule = new PaymentModule({ store, name: "payment" });
export const reviewModule = new ReviewModule({ store, name: "review" });
export const favoritesModule = new FavoritesModule({
  store,
  name: "favorites",
});

export default store;
