import "@/assets/style/main.scss";

import Vue from "vue";
import App from "./App.vue";
import i18n from "./plugins/i18n";
import router from "./router";
import store from "./store";
import WebFont from "webfontloader";
import axios from "axios";
import VueCompositionAPI from "@vue/composition-api";
import BootstrapVue from "bootstrap-vue";
import VueSkeletonLoader from "skeleton-loader-vue";
import SlideUpDown from "vue-slide-up-down";
import StarRating from "vue-star-rating";
import { VueMasonryPlugin } from "vue-masonry";
import CoolLightBox from "vue-cool-lightbox";
import Loader from "@/components/Loader/Loader.vue";
import notice from "@/plugins/notice/index";
import { formatMoney } from "@/helpers/formatters";
import { formatDate, formatDateTime, getDuration } from "@/helpers/date";
import "./registerServiceWorker";

Vue.use(BootstrapVue);
Vue.use(notice);
Vue.use(VueCompositionAPI);
Vue.use(VueSkeletonLoader);
Vue.use(VueMasonryPlugin);
Vue.use(CoolLightBox);
Vue.component("vue-skeleton-loader", VueSkeletonLoader);
Vue.component("slide-up-down", SlideUpDown);
Vue.component("star-rating", StarRating);
Vue.component("Loader", Loader);
Vue.use({
  install(Vue) {
    Vue.prototype.$axios = axios.create({
      baseUrl: process.env.VUE_APP_BASE_URL || "",
    });
  },
});
Vue.prototype.$eventHub = new Vue();

Vue.prototype.$fm = formatMoney;
Vue.prototype.$fd = formatDate;
Vue.prototype.$fdt = formatDateTime;
Vue.prototype.$gd = getDuration;

WebFont.load({
  google: {
    families: ["Roboto:300,400,500,600&display=swap"],
  },
});
Vue.config.productionTip = false;

store.dispatch("Search/getServiceCategories");

new Vue({
  router,
  store,
  i18n,
  render: (h) => h(App),
}).$mount("#app");
