import Cookie from "js-cookie";

export function composeSalonLink(salon) {
  return `/${salon.slug || salon.id}`;
}

export function getCrmRedirectUrl() {
  const savedPath =
    localStorage.getItem("CRM_SAVED_PATH") || Cookie.get("CRM_SAVED_PATH");
  const redirectUrl =
    savedPath && process.env.VUE_APP_CRM_REDIRECT_URL
      ? `${process.env.VUE_APP_CRM_REDIRECT_URL}${savedPath}`
      : process.env.VUE_APP_CRM_REDIRECT_URL ||
        process.env.VUE_APP_HOME_REDIRECT_URL || { name: "Home" };

  return redirectUrl
    .replace(/\/\/+/g, "/")
    .replace(/https?:(\/)/, (match) => `${match}/`);
}
