export function getImageFromSrc(src, returnEmpty = false) {
  if (src) {
    return src.match(/^http/)
      ? src
      : `${process.env.VUE_APP_BASE_URL || ""}${src}`;
  } else {
    return returnEmpty
      ? ""
      : String(require("@/assets/img/no-image.png"));
  }
}
