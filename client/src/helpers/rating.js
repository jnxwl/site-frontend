export function getRatingTitle(rating) {
  if (rating === 0) return "Неизвестно";
  if (rating > 9) return "Очень хорошо";
  if (rating > 7) return "Хорошо";
  if (rating > 4) return "Нормально";
  if (rating > 2) return "Плохо";
  return "Очень плохо";
}
