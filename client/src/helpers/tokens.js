import jwt from "jsonwebtoken";
import jwkToPem from "jwk-to-pem";
import store, { accountModule } from "@/store";
import { authApi } from "@/services/api/auth-api";
import Cookie from "js-cookie";

const getUserId = async (accessToken) => {
  const publicKey = await authApi.getPublicKey()
  return jwt.verify(accessToken, jwkToPem(publicKey.data), {
    algorithm: "RS256",
  });
};

const logout = async () => {
  const refreshToken = Cookie.get("refreshToken");
  store.dispatch("Auth/removeTokens");
  accountModule.signOut();
  await authApi.logout({ refreshToken });
};

const verifyAccessToken = async (accessToken) => {
  let isOk = null;
  const publicKey = await authApi.getPublicKey()
  jwt.verify(
    accessToken,
    jwkToPem(publicKey.data),
    { algorithm: "RS256" },
    function (err) {
      if (err) {
        isOk = err.name;
      } else {
        isOk = { ok: true };
      }
    }
  );
  return isOk;
};

const getNewTokens = async (refreshToken, isFirstTry = true) => {
  const response = await authApi.regenerateTokens({ refreshToken });
  if (response.data.ok) {
    await logout();
    store.dispatch("Auth/setTokens", {
      accessToken: response.data.accessToken,
      refreshToken: response.data.refreshToken,
    });
    return { ok: true };
  } else {
    if (isFirstTry) {
      await getNewTokens(refreshToken, false);
    } else {
      await logout();
    }
  }
  return { ok: false };
};

const isAuth = async () => {
  const verification = await verifyAccessToken(Cookie.get("accessToken"));
  const refreshToken = Cookie.get("refreshToken");
  if (verification) {
    if (verification.ok) {
      store.commit("Auth/changeAuthStatus", { isAuthorized: true });
      return true;
    } else if (verification === "TokenExpiredError" && refreshToken) {
      let newTokens = await getNewTokens(refreshToken);
      let count = 0;
      do {
        count++;
        if (newTokens && newTokens.ok === true) {
          store.commit("Auth/changeAuthStatus", { isAuthorized: true });
          return true;
        }
      } while (count < (process.env.VUE_AUTH_TRY_COUNT || 2));
    }
  }
  return false;
};

export { verifyAccessToken, getNewTokens, isAuth, logout, getUserId };
