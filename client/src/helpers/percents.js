export function getPercentFromNumber(number, percent) {
  return number * (percent / 100) || 0;
}

export function getNumberWithoutPercent(number, percent) {
  return number - getPercentFromNumber(number, percent);
}
