import axios from "axios";
import Cookie from "js-cookie";

class NotyApi {
  constructor() {
    this._axios = axios.create({
      baseURL: `${process.env.VUE_APP_BASE_URL || ""}/noty/api/v1/`,
      headers: { Accept: "application/json" },
    });
  }

  getUnreadMessagesCount() {
    return this._axios.post("getUnreadMessagesCount", {
      access_token: Cookie.get("accessToken"),
    });
  }

  getInboxMessages(opts) {
    return this._axios.post("getInboxMessages", {
      ...opts,
      access_token: Cookie.get("accessToken"),
    });
  }

  markMessageRead(opts) {
    return this._axios.post("markMessageRead", {
      ...opts,
      access_token: Cookie.get("accessToken"),
    });
  }
}

export const notyApi = new NotyApi();
