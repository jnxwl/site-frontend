import { composeUriQuery } from "@/helpers/uri-query";
import axios from "axios";
import Cookie from "js-cookie";

class MainApi {
  constructor() {
    this._axios = axios.create({
      baseURL: `${process.env.VUE_APP_BASE_URL || ""}/main-back/api/v1/`,
      headers: { Accept: "application/json" },
    });
  }

  getSalonTypes() {
    return this._axios.get("salon-types");
  }

  getSalonServices(id, query = {}) {
    return this._axios.get(`salon-services/${id}?${composeUriQuery(query)}`);
  }

  getServiceCategories(query = {}) {
    return this._axios.get(`categories?${composeUriQuery(query)}`);
  }

  getSalons(query = {}) {
    return this._axios.get(`salons?${composeUriQuery(query)}`);
  }

  getServices(query = {}) {
    return this._axios.get(`services?${composeUriQuery(query)}`);
  }

  getGallery(id) {
    return this._axios.get(`gallery/${id}`);
  }

  getSpecialistServices(query = {}) {
    return this._axios.get(`specialist-services?${composeUriQuery(query)}`);
  }

  getAddressFromIp(query = {}) {
    return this._axios.get(`get-address-from-ip?${composeUriQuery(query)}`);
  }

  getBodyParts() {
    return this._axios.get("body-parts");
  }

  searchSalons(query) {
    return this._axios.get(`search?${composeUriQuery(query)}`);
  }

  searchMapSalons(query) {
    return this._axios.get(`search-map?${composeUriQuery(query)}`);
  }

  searchServices(query) {
    return this._axios.get(`search-service?${composeUriQuery({
      ...query,
      is_confirmed: 1,
    })}`);
  }

  getSalonById(id) {
    return this._axios.get(`salons/${id}`);
  }

  getCompanyById(id) {
    return this._axios.get(`companies/${id}`);
  }

  createOrder(order) {
    return this._axios.post("orders", order, {
      headers: {
        Authorization: `Bearer ${Cookie.get("accessToken")}`,
      },
    });
  }

  registerCompany(params) {
    return this._axios.post("companies", params);
  }

  getRates() {
    return this._axios.get("rates");
  }

  getSpecialists(query) {
    return this._axios.get(`specialists?${composeUriQuery(query)}`);
  }

  getSpecialist(specialistId) {
    return this._axios.get(`specialists/${specialistId}`);
  }

  getFreeTime(query) {
    return this._axios.get(`specialists/free-time?${composeUriQuery(query)}`, {
      headers: {
        Authorization: `Bearer ${Cookie.get("accessToken")}`,
      },
    });
  }

  allowOrder(query) {
    return this._axios.get(
      `specialists/allow-order?${composeUriQuery(query)}`,
      {
        headers: {
          Authorization: `Bearer ${Cookie.get("accessToken")}`,
        },
      }
    );
  }

  createImage(file) {
    const form = new FormData();
    form.append("file", file);

    return this._axios.post("images", form, {
      headers: {
        "Content-Type": "multipart/form-data",
        Authorization: `Bearer ${Cookie.get("accessToken")}`,
      },
    });
  }

  getReviews(query) {
    return this._axios.get(`reviews?${composeUriQuery(query)}`);
  }

  createReview(body) {
    return this._axios.post("reviews", body, {
      headers: {
        Authorization: `Bearer ${Cookie.get("accessToken")}`,
      },
    });
  }

  getReviewsStatistics(params) {
    return this._axios.get(`reviews/statistic`, { params });
  }

  getOrders(query) {
    return this._axios.get(`orders?${composeUriQuery(query)}`, {
      headers: {
        Authorization: `Bearer ${Cookie.get("accessToken")}`,
      },
    });
  }

  getOrder(id) {
    return this._axios.get(`orders/${id}`, {
      headers: {
        Authorization: `Bearer ${Cookie.get("accessToken")}`,
      },
    });
  }

  updateOrder(id, data) {
    return this._axios.put(`orders/${id}`, data, {
      headers: {
        Authorization: `Bearer ${Cookie.get("accessToken")}`,
      },
    });
  }

  cancelOrder(id) {
    return this._axios.post(
      `orders/user-cancel/${id}`,
      {},
      {
        headers: {
          Authorization: `Bearer ${Cookie.get("accessToken")}`,
        },
      }
    );
  }

  complainOrder(id) {
    return this._axios.post(
      `orders/complain/${id}`,
      {},
      {
        headers: {
          Authorization: `Bearer ${Cookie.get("accessToken")}`,
        },
      }
    );
  }

  getBlogs(query = {}) {
    return this._axios.get(`blogs?${composeUriQuery(query)}`);
  }

  getBlog(id) {
    return this._axios.get(`blogs/${id}`);
  }
}

export const mainApi = new MainApi();
