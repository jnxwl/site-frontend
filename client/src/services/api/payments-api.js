import axios from "axios";
import Cookie from "js-cookie";

class PaymentsApi {
  constructor() {
    this._axios = axios.create({
      baseURL: `${process.env.VUE_APP_BASE_URL || ""}/payment-back/api/v1`,
      headers: { Accept: "application/json" },
    });
  }

  getPayments(body) {
    return this._axios.post("getPayments", body);
  }

  createPayment(body) {
    return this._axios.post("createPayment", body);
  }

  getTransactions(body) {
    return this._axios.post("getTransactions", body);
  }

  addTransaction(body) {
    return this._axios.post("addTransaction", body);
  }

  getBalance(body) {
    return this._axios.post("getBalance", body);
  }

  getAccountStatus(body) {
    return this._axios.post("getAccountStatus", body);
  }

  getCashbackSettings() {
    return this._axios.post("getCashbackSettings", {});
  }

  getSpentAmount(body) {
    return this._axios.post("getSpentAmount", body);
  }

  withdrawBalance(body) {
    return this._axios.post("withdrawBalance", body);
  }

  topUpBalance(body) {
    return this._axios.post("topUpBalance", body);
  }

  getSavedCards() {
    return this._axios.post("getSavedCards", {
      access_token: Cookie.get("accessToken"),
    });
  }

  setPrimaryCard(id) {
    return this._axios.post("makeSavedCardPrimary", {
      access_token: Cookie.get("accessToken"),
      payment_id: id,
    });
  }
}

export const paymentsApi = new PaymentsApi();
