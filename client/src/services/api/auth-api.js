import axios from "axios";
import Cookie from "js-cookie";

class AuthApi {
  constructor() {
    this._axios = axios.create({
      baseURL: `${process.env.VUE_APP_BASE_URL || ""}/auth-back/api/v2/`,
      headers: { Accept: "application/json" },
    });
  }

  getPublicKey() {
    return this._axios.post("getPublicKey", {});
  }

  getRecaptchaSiteKey() {
    return this._axios.post("getRecaptchaSiteKey");
  }

  login(params) {
    return this._axios.post("login", params);
  }

  logout(params) {
    return this._axios.post("logout", params);
  }

  regenerateTokens(params) {
    return this._axios.post("regenerateTokens", params);
  }

  getInfoFromInn(params) {
    return this._axios.post("getLegalEntityInfo", params);
  }

  getOAuthLink(params) {
    return this._axios.post("getOAuthLink", params);
  }

  oauthLogin(params) {
    return this._axios.post("oauthLogin", params);
  }

  registerClient(params) {
    return this._axios.post("register", params);
  }

  registerBusiness(params) {
    return this._axios.post("register", params);
  }

  sendConfirmCode(params) {
    return this._axios.post("sendConfirmCode", params);
  }

  resetPassword(params) {
    return this._axios.post("resetPassword", params);
  }

  getUserInfo(params = {}) {
    return this._axios.post("getUserInfo", {
      ...params,
      accessToken: Cookie.get("accessToken"),
    });
  }

  updateUserInfo(params = {}) {
    return this._axios.post("updateUserInfo", {
      ...params,
      accessToken: Cookie.get("accessToken"),
    });
  }

  changeLogin(params = {}) {
    return this._axios.post("changeLogin", {
      ...params,
      accessToken: Cookie.get("accessToken"),
    });
  }
}

export const authApi = new AuthApi();
