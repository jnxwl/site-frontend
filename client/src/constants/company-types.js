export const COMPANY_TYPES_MAP = Object.freeze({
  legalEntity: 1,
  individualEntrepreneur: 2,
  selfEmployed: 3,
});
