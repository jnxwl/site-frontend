export const USER_STATUSES = {
  classic: "classic",
  silver: "silver",
  gold: "gold",
  platinum: "platinum",
};

export const STATUS_IMAGES_MAP = {
  [USER_STATUSES.classic]: require("@/assets/img/status/steps-bg1.png"),
  [USER_STATUSES.silver]: require("@/assets/img/status/steps-bg2.png"),
  [USER_STATUSES.gold]: require("@/assets/img/status/steps-bg3.png"),
  [USER_STATUSES.platinum]: require("@/assets/img/status/steps-bg4.png"),
};
