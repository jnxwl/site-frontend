export class Company {
  constructor(raw = {}) {
    this.id = Number(raw.id || 0);
    this.inn = Number(raw.inn || 0);
    this.isConfirmed = Boolean(raw.is_confirmed);
    this.type = Number(raw.type || 0);
  }
}
