import { getImageFromSrc } from "@/helpers/images";

export class GalleryImage {
  constructor(raw = {}) {
    this.id = Number(raw.id);
    this.name = getImageFromSrc(raw.name, true);
    this.userId = String(raw.user_id);
  }
}
