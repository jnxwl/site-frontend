import i18n from "@/plugins/i18n.js";

export class Note {
  constructor(raw = {}) {
    this.id = String(raw.id);
    this.salonId = Number(raw.salon_id) || 0;

    this.createdAt = new Date(raw.created_at || Date.now());
    this.isRead = Boolean(raw.read);
    this.sub = String(raw.sub || "");

    this.subject = String(raw.subject || "");
    this.text = String(raw.msg || "").replace(/\*\*(.*?)\*\*/, (match, p) => {
      return i18n.t(`notes.${p}`);
    });
  }

  get route() {
    if (this.salonId) {
      return {
        name: "Salon",
        params: { id: this.salonId },
      };
    }

    return null;
  }

  get isBooking() {
    return Boolean(this.subject === "salon" && this.salonId);
  }
}
