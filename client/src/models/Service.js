import { getRatingTitle } from "@/helpers/rating";
import { Category } from "./Category";

export class Service {
  constructor(raw = {}) {
    this.id = Number(raw.id);
    this.title = String(raw.title || "");

    this.category = new Category(raw.category);
    this.categoryId = Number(this.category.id || raw.category_id);
    this.categoryTitle = this.category.title;

    this.reviewsCount = Number(raw.review_count || 0);
    this.rating = Number(raw.rating || 0);
    this.ratingTitle = getRatingTitle(this.rating);
    this.imageSrc = require("@/assets/img/no-image.png");

    const categoryData = raw.category?.data || {};
    this.categoryIcon = categoryData.img || "";
  }
}
