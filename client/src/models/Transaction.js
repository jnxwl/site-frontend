export class Transaction {
  constructor(raw = {}) {
    this.id = String(raw.id || "");
    this.createdAt = new Date(raw.created_at);
    if (isNaN(this.createdAt)) this.createdAt = null;
    this.status = String(raw.status || "");
    this.amount = Number(raw.amount || 0);
    this.cashback = Number(raw.cashback || 0);

    const metadata = raw.metadata || {};
    this.salonId = Number(metadata.salon_id || 0);
    this.salonName = String(metadata.salon_name || "");
    this.serviceName = String(metadata.service_name || "");
  }
}
