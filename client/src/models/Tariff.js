export class Tariff {
  constructor(raw = {}) {
    this.id = Number(raw.id || 0);

    this.price = Number(raw.price || 0);
    this.status = Number(raw.status || 0);
    this.title = String(raw.title || "");

    this.createdAt = new Date(raw.created_at);
    this.updatedAt = new Date(raw.updated_at);

    const data = raw.data || {};
    this.description = String(data.description || "");
  }
}
