import { getImageFromSrc } from "@/helpers/images";

export class BlogArticle {
  constructor(raw = {}) {
    this.id = raw.id || 0;
    this.date = new Date(raw.created_at);
    this.title = raw.title || "";
    this.description = raw.description || "";

    this.image = getImageFromSrc(raw.image);

    const data = raw.data || {};
    this.tags = data.categories || [];
  }
}
