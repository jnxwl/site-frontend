/* eslint-disable no-console */

workbox.core.setCacheNameDetails({ prefix: "webberu-pwa" });

self.__precacheManifest = [].concat(self.__precacheManifest || []);
workbox.precaching.precacheAndRoute(self.__precacheManifest, {});

self.addEventListener("message", (event) => {
  if (event.data && event.data.type === "SKIP_WAITING") {
    self.skipWaiting();
  }
});

self.addEventListener("activate", function (event) {
  console.log("[Service Worker] Activating Service Worker ....");

  const registration = event.target.registration;
  const channel = new BroadcastChannel("notification-messages");
  let webSocket = null;

  function connectNotifications() {
    const pathToJson = new URL(location).searchParams.get("notificationsUri");
    const wsUrl =
      pathToJson ||
      location.origin.replace("http", "ws") + "/noty/notifications";
    console.log("Connection from service worker to ", wsUrl);
    webSocket = new WebSocket(wsUrl);

    webSocket.onmessage = function (e) {
      const data = JSON.parse(e.data);
      switch (data.type) {
        case "id":
          channel.postMessage(data);
          break;
        case "msg":
          if (Notification.permission === "granted") {
            registration.showNotification(data.msg);
          } else {
            channel.postMessage(data);
          }
          break;
        case "response":
          if (data.ok === false) {
            throw Error(data.error);
          }
          break;
        case "inbox":
          eventHandlers["notification:new_inbox_message"].forEach((h) => h());
          break;
      }
    };

    webSocket.onclose = function (e) {
      console.log(
        "Socket is closed. Reconnect will be attempted in 1 second.",
        e.reason
      );
      setTimeout(function () {
        connectNotifications();
      }, 2500);
    };

    webSocket.onerror = function (err) {
      console.error(
        "Socket encountered error: ",
        err.message,
        "Closing socket"
      );
      webSocket.close();
    };
  }

  connectNotifications();
});
