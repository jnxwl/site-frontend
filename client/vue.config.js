const CopyWebpackPlugin = require("copy-webpack-plugin");
const UnusedWebpackPlugin = require("unused-webpack-plugin");
const path = require("path");
const proxyUrl =
  process.env.VUE_APP_PROXY_URL || "https://webberu.dev-webdevep.ru";

module.exports = {
  runtimeCompiler: true,
  productionSourceMap: true,
  lintOnSave: process.env.NODE_ENV !== "production",
  filenameHashing: true,
  pages: {
    index: {
      entry: "src/main.js",
    },
  },
  devServer: {
    port: 5000,
    hot: true,
    headers: {
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE, PATCH, OPTIONS",
      "Access-Control-Allow-Headers":
        "X-Requested-With, content-type, Authorization",
    },
    historyApiFallback: true,
    disableHostCheck: true,
    watchOptions: {
      ignored: /node_modules/,
      poll: 1000,
    },
    proxy: {
      "^/auth-back": {
        target: proxyUrl,
        ws: true,
        secure: false,
        changeOrigin: true,
      },
      "^/main-back": {
        target: proxyUrl,
        secure: false,
        changeOrigin: true,
      },
      "^/payment-back": {
        target: proxyUrl,
        secure: false,
        changeOrigin: true,
      },
      "^/noty": {
        target: proxyUrl,
        secure: false,
        changeOrigin: true,
      },
      "^/chat-server": {
        target: proxyUrl,
        ws: true,
        secure: false,
        changeOrigin: true,
      },
    },
  },
  configureWebpack: {
    devtool: "eval-source-map",
    plugins: [
      new UnusedWebpackPlugin({
        directories: [path.join(__dirname, "src")],
      }),
      new CopyWebpackPlugin([
        process.env.VUE_APP_ROBOTS === "prod"
          ? { from: "public/robots.prod.txt", to: "robots.txt" }
          : { from: "public/robots.stage.txt", to: "robots.txt" }
      ]),
    ],
    externals: {
      vue: "Vue",
      "vue-router": "VueRouter",
      vuex: "Vuex",
      "vue-i18n": "VueI18n",
      bootstrap: "bootstrap",
      "bootstrap-vue": "BootstrapVue",
      moment: "moment",
      lodash: "_",
      axios: "axios",
      swiper: "Swiper",
    },
    output: {
      filename: '[name].[hash].js',
    },
  },
  pwa: {
    name: "Маркетплейс Webberu",
    themeColor: "#6182f7",
    appleMobileWebAppCapable: "yes",
    appleMobileWebAppStatusBarStyle: "black",

    // настройки манифеста
    manifestOptions: {
      short_name: "WebbMP",
      display: "standalone",
      background_color: "#6182f7",
      scope: "/",
      start_url: "/",
      description: "Маркетплейс Webberu",
    },
    workboxPluginMode: "InjectManifest",
    workboxOptions: {
      swSrc: "sw.js",
      globDirectory: "./dist",
      globPatterns: [
        "**/*.js",
        "**/*.css",
        "**/*.svg",
      ],
    },
  },
  css: {
    loaderOptions: {
      sass: {
        additionalData: `@import "~@/assets/style/_variables.scss";`,
      },
    },
  },
  transpileDependencies: ["vuex-module-decorators"],
};
