#!/usr/bin/env bash
echo "++++++ get from site-back git ++++++"
git stash
git fetch origin
git checkout origin/dev
git pull origin dev

echo "++++++ upgrade npm packages ++++++"
docker-compose exec client yarn upgrade

echo "++++++ purge cache ++++++"
curl -X POST "https://api.cloudflare.com/client/v4/zones/8863666c199d9d06107677df4b3ad1d0/purge_cache" \
     -H "Authorization: Bearer $1" \
     -H "Content-Type: application/json" \
     --data "{ \"purge_everything\": true }"

echo "++++++ regenerate prj docs ++++++"
cd ../docs || exit
docker-compose stop client
docker-compose up -d
docker system prune -a -f
