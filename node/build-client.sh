#!/usr/bin/env bash
echo "### start"
rm -rf dist
echo "### install node modules"
yarn install
if [[ $IS_LOCAL == true && $MODE == 'development' ]]
then
  echo "### Build single-spa dev client"
  yarn dev:single
  exit
elif [ $MODE == 'development' ]
then
  echo "### Build dev client without single-spa"
  yarn dev
  exit
fi
if [[ $IS_LOCAL == true && $MODE == 'production' ]]
then
  echo "### Build single-spa prod client"
  mkdir -p dist
  yarn build:single
  rm -rf dist/index.html
  exit
fi
echo "### Build prod client without single-spa"
mkdir -p dist
yarn build
